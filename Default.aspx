﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>Dashboard 
    </h2>
    <h3 class="projects-by-country">Projects by Country</h3>
     
  
    <div class="click-a-country">Click Countries for Reports</div>
    <div id="map"></div>

    <p>
        <asp:RadioButtonList ID="rblReport" runat="server" RepeatDirection="Horizontal">
            <asp:ListItem Text="Report in Word" Value="1"></asp:ListItem>
            <asp:ListItem Text="Report in Excel" Value="2"></asp:ListItem>
        </asp:RadioButtonList>
    </p>
    <table id="Table1" runat="server" class="table">
        <tr>
            <td>
                <asp:LinkButton ID="lnkRegional" runat="server" OnClick="lnkRegional_Click">Regional</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkAlgeria" runat="server" OnClick="lnkAlgeria_Click">Algeria</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkBahrain" runat="server" OnClick="lnkBahrain_Click">Bahrain</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkEgypt" runat="server" OnClick="lnkEgypt_Click">Egypt</asp:LinkButton>
            </td>
            <td>
                <asp:LinkButton ID="lnkIran" runat="server" OnClick="lnkIran_Click">Iran</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkIraq" runat="server" OnClick="lnkIraq_Click">Iraq</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkIsrael" runat="server" OnClick="lnkIsrael_Click"> Israel</asp:LinkButton>
            </td>
        </tr>

        <tr>

            <td>
                <asp:LinkButton ID="lnkJordan" runat="server" OnClick="lnkJordan_Click">Jordan</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkKuwait" runat="server" OnClick="lnkKuwait_Click">Kuwait</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkLebanon" runat="server" OnClick="lnkLebanon_Click">Lebanon</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkLibya" runat="server" OnClick="lnkLibya_Click">Libya</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkMorocco" runat="server" OnClick="lnkMorocco_Click">Morocco</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkOman" runat="server" OnClick="lnkOman_Click">Oman</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkQatar" runat="server" OnClick="lnkQatar_Click">Qatar</asp:LinkButton></td>
        </tr>

        <tr>

            <td>
                <asp:LinkButton ID="lnkTurkey" runat="server" OnClick="lnkTurkey_Click">Turkey</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkUAE" runat="server" OnClick="lnkUAE_Click">UAE</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkSaudiArabia" runat="server" OnClick="lnkSaudiArabia_Click">Saudi Arabia</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkSyria" runat="server" OnClick="lnkSyria_Click">Syria</asp:LinkButton>
            </td>
            <td>
                <asp:LinkButton ID="lnkTunisia" runat="server" OnClick="lnkTunisia_Click">Tunisia</asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkWestBankGaza" runat="server" OnClick="lnkWestBankGaza_Click">West Bank/Gaza </asp:LinkButton></td>
            <td>
                <asp:LinkButton ID="lnkYemen" runat="server" OnClick="lnkYemen_Click">Yemen</asp:LinkButton>
            </td>
        </tr>


    </table>




    <ul>

        <%-- <li>!Report 2  <a href="">pdf</a></li> --%>
    </ul>
    <%-- <h3>Projects by Sector</h3>
    <ul>
        <li>!Report 1  <a href="">pdf</a></li>
        <li>!Report 2  <a href="">pdf</a></li> 
    </ul> 
    <h3>Projects by Agency</h3>--%>
    <%--<ul>
        <li>!Expenditure Report <a href="">pdf</a></li>
 
    </ul> --%>
    <div class="report-contain">
        <div class="report-area">
            <h3>Reports</h3>
            <!-- <div class="Tblnew">
    <ul  >
        
        <li> <a href="Reports/ReportsByCountry.aspx">Report By Country, Program Area,  Agency</a>  </li>
         <li> <a href="Reports/ReportsBySector.aspx">Report By Sector</a></li>
        <li><a href="ReportCustomGen.aspx">Generate custom report</a></li>
         <li><a href="Reports/ReportsByStatus.aspx">Generate status report</a></li>
        
    </ul> -->

            <!-- icons -->
            <div class="icons">
                <div class="col-xs-3 text-center zoom_img ">
                    <a href="Reports/ReportsByCountry.aspx">
                        <img src="images/report-countries.png" alt="Reports by Country">
                        <p>Report By Country, Program Area, Agency</p>
                    </a>
                </div>
                <div class="col-xs-3 text-center zoom_img">
                    <a href="Reports/ReportsBySector.aspx">
                        <img src="images/report-by-sector.png" alt="Reports by Sector">
                        <p>Report By Sector</p>
                    </a>
                </div>
                <div class="col-xs-3 text-center zoom_img">
                    <a href="ReportCustomGen.aspx">
                        <img src="images/report-custom.png" alt="">
                        <p>Generate Custom Report</p>
                    </a>
                </div>
                <div class="col-xs-3 text-center zoom_img">
                    <a href="Reports/ReportsByStatus.aspx">
                        <img src="images/report-status.png" alt="">
                        <p>Generate Status Report</p>
                    </a>
                </div>
            </div>
            <!-- end icons -->
        </div>
    </div>
    <h3 class="clear">Project Details</h3>
    <br />
    <a href="ProjectsList.aspx">Project List</a>
    <%-- <li><a href="">!Missing Data Reports </a></li>--%>
    </ul>

     </div>  
     
    <script src="scripts/mapdata.js"></script>
    <script src="scripts/worldmap.js"></script>
</asp:Content>

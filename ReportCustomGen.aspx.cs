﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.IO;
using DocumentFormat.OpenXml;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;


public partial class ReportCustomGen : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["MEPI_ProjectsConnectionString"].ToString();
   
    string sFields = "";
    string sFilters = "";

      string sFilterCountry = "";
    string sFilterAgency = "";
     string sFilterProgramArea = "";
     string sFilterSector= "";

    string sFilterPrimeParner = "";
    string sFilterSubImplementingPartner = "";

    DateTime? sFilterStartFrom;
   DateTime? sFilterStartTo;
   DateTime? sFilterEndFrom;
   DateTime? sFilterEndTo;
   SectorsRep rep1 = new SectorsRep();
   int test;
   Int64 res;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            chkFieldAgency.Checked = true;
            chkFieldFundingAcc.Checked = true;
            chkFieldTotalFunding.Checked = true;
            chkFieldPrjTimeline.Checked = true;
            chkFieldPrimePartner.Checked = true;
            chkFieldProjectDescription.Checked = true;

            //Enabled filters when checkbox associated to it is checked
            ddlCountry.Enabled = false;
            cboAgency.Enabled = false;
            cboProgramAreas.Enabled = false;
            cboSector.Enabled = false;
            txtPrimePartner.Enabled = false;
            txtSubImplementingPartner.Enabled = false;
            txtStartDateFrom.Enabled = false;
            txtStartDateTo.Enabled = false;
            txtEndDateFrom.Enabled = false;
            txtEndDateTo.Enabled = false;

            loadDropdown();
        }
        Page.MaintainScrollPositionOnPostBack = true;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        getFields();
        getFilters();
        genDocument();

    }
    protected void getFields()
    {
        sFields = "";
        for (int i = 0; i < chkOutputfields.Items.Count; i++)
        {
            if (chkOutputfields.Items[i].Selected == true)
            {
                if (sFields != "")
                {
                    sFields = sFields + "," + chkOutputfields.Items[i].Value;
                }
                else
                {
                    sFields = sFields + chkOutputfields.Items[i].Value;
                }
             }
        }            
    }
    protected void getFilters()
    {
         sFilters = "";
         sFilterCountry = "";
         sFilterAgency = "";
         sFilterCountry = "";
         sFilterProgramArea = "";
         sFilterSector = "";    
         sFilterPrimeParner = "";
         sFilterSubImplementingPartner = "";
         

        if (chkAgency.Checked == true)
        {
            if( cboAgency.SelectedIndex!=-1 && cboAgency.SelectedValue!="")
            {
                sFilters = " AND " + "AgencyAbbr='" + cboAgency.SelectedValue + "'";
                sFilterAgency = cboAgency.SelectedItem.Text;
            }
        }
        if (chkCountry.Checked == true)
        {
            if (ddlCountry.SelectedIndex != -1 && ddlCountry.SelectedValue != "")
            {
                sFilters = " AND " + "AgencyAbbr='" + ddlCountry.SelectedValue + "'";
                sFilterCountry = ddlCountry.SelectedItem.Text ;
            }
        }
        if (chkProgramArea.Checked == true)
        {
            if (cboProgramAreas.SelectedIndex != -1 && cboProgramAreas.SelectedValue != "")
            {
                sFilterProgramArea = cboProgramAreas.SelectedItem.Text;
            }
        }

        if (chkSector.Checked == true)
        {
            if (cboSector.SelectedIndex != -1 && cboSector.SelectedValue != "")
            {
                sFilterSector = cboSector.SelectedItem.Text;
            }
        }

        if (chkPrimeParner.Checked == true)
        {
            sFilters = sFilters + " AND " + "PrimePartner='" + txtPrimePartner.Text + "'";
            sFilterPrimeParner = txtPrimePartner.Text;
        }

        // added on Feb 26
        if (chkSubImplementingPartner.Checked == true)
        {
            sFilterSubImplementingPartner = txtSubImplementingPartner.Text;
        }

        if (chkStartDate.Checked == true)
        {

            if (txtStartDateFrom.Text != "")
            {
                sFilterStartFrom = Convert.ToDateTime(txtStartDateFrom.Text);
            }

            if (txtStartDateTo.Text != "")
            {
                sFilterStartTo = Convert.ToDateTime(txtStartDateTo.Text);
            }

        }

        if (chkEndDate.Checked == true)
        {
            if (txtEndDateFrom.Text != "")
            {
                sFilterEndFrom = Convert.ToDateTime(txtEndDateFrom.Text);
            }

            if (txtEndDateTo.Text != "")
            {
                sFilterEndTo = Convert.ToDateTime(txtEndDateTo.Text);
            }
        }


        if (sFilters!="")
        {
            char[] charsToTrim = { ',', ' ' };
            sFilters = sFilters.Substring(5);
        }
        

       
    }

    ///////////////////////////////////////////////
    protected void loadDropdown()
    {

        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.CountriesBMENAs
                      select e;
            ddlCountry.DataSource = qry;
            ddlCountry.DataTextField = "Countryname";
            ddlCountry.DataValueField = "Id";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, "");

            var qry4 = from e in db.Agencies
                       orderby e.AgencyAbbr
                       select e;
            cboAgency.DataSource = qry4;
            cboAgency.DataTextField = "AgencyAbbr";
            cboAgency.DataValueField = "AgencyID";
            cboAgency.DataBind();
            cboAgency.Items.Insert(0, "");

            var qry5 = from e in db.ProgramAreas                       
                       orderby e.ProgramArea1
                       select e;
            cboProgramAreas.DataSource = qry5;
            cboProgramAreas.DataTextField = "ProgramArea1";
            cboProgramAreas.DataValueField = "ProgramAreaID";
            cboProgramAreas.DataBind();
            cboProgramAreas.Items.Insert(0, "");

            var qry6 = from e in db.Sectors
                       orderby e.Sector1
                       select e;
            cboSector.DataSource = qry6;
            cboSector.DataTextField = "Sector1";
            cboSector.DataValueField = "SectorID";
            cboSector.DataBind();
            cboSector.Items.Insert(0, "");

        }
    }
//----///
    public  void WriteToWordDoc(string filepath)
    {
        string sTmpCountry = "";
        string sTmpProjectName = "";
        string sTmpProgramArea = "";
        // Open a WordprocessingDocument for editing using the filepath.
        using (WordprocessingDocument wordprocessingDocument =
             WordprocessingDocument.Open(filepath, true))
        {
            // Assign a reference to the existing document body.
            Body body = wordprocessingDocument.MainDocumentPart.Document.Body;

            using (DataClasses2DataContext db = new DataClasses2DataContext())
            {
            /////////////////////////////////////////////////////////

            //Project Information

          
            Paragraph wrdDate = new Paragraph(
            new ParagraphProperties(
                new ParagraphStyleId() { Val = "Heading4" }),
            new Run(new Text(" Report Date: " + String.Format("{0:MMMM-d, yyyy}", DateTime.Now))));
            body.Append(wrdDate);


            // Paragraph wrdCount = new Paragraph(
            //new ParagraphProperties(
            //    new ParagraphStyleId() { Val = "Heading4" }),
            //new Run(new Text(" Project Count: " + oPages)));
            // body.Append(wrdCount);

            // Paragraph wrdSumTFunding = new Paragraph(
            //new ParagraphProperties(
            //    new ParagraphStyleId() { Val = "Heading4" }),
            //new Run(new Text("Total Funding: " + String.Format("{0:C}", oSum))));
            // body.Append(wrdSumTFunding);

            //---- count
            getCount();
            //var oProjects1 = db.get_ProjectListByCountryPAreaAgency_X(sFilterCountry, sFilterPrimeParner, sFilterProgramArea, sFilterSubImplementingPartner, sFilterAgency, sFilterSector, sFilterStartFrom, sFilterStartTo, sFilterEndFrom, sFilterEndTo);
         
            //int test = oProjects1.Count();

            Paragraph wrdCount = new Paragraph(
            new ParagraphProperties(
            new ParagraphStyleId() { Val = "Heading4" }),
            new Run(new Text(" Project Count: " + test)));
            body.Append(wrdCount);

            //---Sum Total funding

            getTotalFunding();

            

            Paragraph wrdSumTFunding = new Paragraph(
           new ParagraphProperties(
               new ParagraphStyleId() { Val = "Heading4" }),
           new Run(new Text("Total Funding: " + String.Format("{0:C}", res))));
            body.Append(wrdSumTFunding);

            //--------------


            /////////////
            

              //  var oProjects = db.get_ProjectListByCountryPArea(sCountry, sProgramArea, sAgency);
               // var oProjects = db.get_ProjectListByCountryPArea_X(sFilterCountry, sFilterProgramArea, sFilterSubImplementingPartner, sFilterAgency, sFilterSector, sFilterStartFrom, sFilterStartTo, sFilterEndFrom, sFilterEndTo);
                var oProjects = db.get_ProjectListByCountryPAreaAgency_X(sFilterCountry, sFilterPrimeParner, sFilterProgramArea, sFilterSubImplementingPartner, sFilterAgency, sFilterSector, sFilterStartFrom, sFilterStartTo, sFilterEndFrom, sFilterEndTo);
                

                foreach (get_ProjectListByCountryPAreaAgency_XResult oProject in oProjects.ToList())
                {
                    if (sTmpCountry != oProject.Countryname)
                    {
                        sTmpCountry = oProject.Countryname;


                        Paragraph wrdParaCoutry = new Paragraph(
                            new ParagraphProperties(
                                new ParagraphStyleId() { Val = "Title" }),
                            new Run(
                               new Text("Projects in " + sTmpCountry)));
                                 
                        body.Append(wrdParaCoutry);
                        /////////////////////////////////////////////////////////
                    }


                    if (sTmpProgramArea != oProject.ProgramArea)
                    {
                        sTmpProgramArea = oProject.ProgramArea;

                        Paragraph newParaProgramArea = new Paragraph(
                            new ParagraphProperties(
                                new ParagraphStyleId() { Val = "Heading1" }),
                            new Run(
                                new Text("Program Area: " + sTmpProgramArea)));
                        body.Append(newParaProgramArea);
                        /////////////////////////////////////////////////////////
                    }

                    if (sTmpProgramArea != oProject.ProgramArea)
                    {
                        sTmpProgramArea = oProject.ProgramArea;

                        Paragraph newParaProgramArea = new Paragraph(
                            new ParagraphProperties(
                                new ParagraphStyleId() { Val = "Heading2" }),
                            new Run(
                                new Text("Active Projects in" + sTmpProgramArea)));
                        body.Append(newParaProgramArea);
                        /////////////////////////////////////////////////////////
                    }

                    //Project Information
                    Paragraph wrdParaProject = new Paragraph(
                    new ParagraphProperties(
                        new ParagraphStyleId() { Val = "Heading3" }),
                    new Run(
                        new Text(oProject.ProjectName)));
                    body.Append(wrdParaProject);



                    Table table = new Table();
                    //TableProperties tblProp = new TableProperties(
                    //    new TableBorders(
                    //        new TopBorder() { Val = new EnumValue<BorderValues>(BorderValues.None ) },
                    //        new BottomBorder() { Val = new EnumValue<BorderValues>(BorderValues.None)  },
                    //        new LeftBorder() { Val = new EnumValue<BorderValues>(BorderValues.None)  },
                    //        new RightBorder() { Val = new EnumValue<BorderValues>(BorderValues.None)  },
                    //        new InsideHorizontalBorder() { Val = new EnumValue<BorderValues>(BorderValues.None)  },
                    //        new InsideVerticalBorder() { Val = new EnumValue<BorderValues>(BorderValues.None) }
                    //    )
                    //);
                    //// Append the TableProperties object to the empty table.
                    //table.AppendChild<TableProperties>(tblProp);


                    TableProperties tblPr = new TableProperties();
                    TableStyle tblStyle = new TableStyle();
                    tblStyle.Val = "LightShading-Accent1";
                    tblPr.AppendChild(tblStyle);
                    table.AppendChild(tblPr);

                    if (chkFieldAgency.Checked == true)
                    {
                        //-----------First Row
                        TableRow tr = new TableRow();
                        TableCell tc1 = new TableCell();
                        tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                        tc1.Append(new Paragraph(new Run(new Text("USG Agency/Office: "))));
                        tr.Append(tc1);

                        TableCell tc2 = new TableCell();
                        tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                        tc2.Append(new Paragraph(new Run(new Text(oProject.AgencyAbbr))));
                        tr.Append(tc2);
                        table.Append(tr);
                    }
                    //-----------Second Row
                    if (chkFieldTotalFunding.Checked == true)
                    {
                        TableRow tr2 = new TableRow();
                        TableCell tr2tc1 = new TableCell();
                        tr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                        tr2tc1.Append(new Paragraph(new Run(new Text("Total Funding:"))));
                        tr2.Append(tr2tc1);

                        TableCell tr2tc2 = new TableCell();
                        tr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                        tr2tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:C}", oProject.TotalFunding)))));
                        tr2.Append(tr2tc2);

                        table.Append(tr2);
                    }
                    //-----------------------------
                    //-----------Funding Account Row
                    if (chkFieldFundingAcc.Checked == true)
                    {
                        TableRow tr3 = new TableRow();
                        TableCell tr3tc1 = new TableCell();
                        tr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                        tr3tc1.Append(new Paragraph(new Run(new Text("Funding Account:"))));
                        tr3.Append(tr3tc1);

                        TableCell tr3tc2 = new TableCell();
                        tr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));

                        //tr3tc2.Append(new Paragraph(new Run(new Text(oProject.FundingAccount.ToString()))));
                        //tr3.Append(tr3tc2);

                        if (oProject.FundingAccount!=null)
                        {
                            tr3tc2.Append(new Paragraph(new Run(new Text(oProject.FundingAccount.ToString()))));
                            tr3.Append(tr3tc2);
                        }
                        else
                        {
                            tr3tc2.Append(new Paragraph(new Run(new Text(""))));
                            tr3.Append(tr3tc2);

                        }

                        table.Append(tr3);
                    }
                    //-----------------------------
                    //-----------Project Timeline   Row
                    if (chkFieldPrjTimeline.Checked == true)
                    {
                        TableRow tr4 = new TableRow();
                        TableCell tr4tc1 = new TableCell();
                        tr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                        tr4tc1.Append(new Paragraph(new Run(new Text("Project Timeline:"))));
                        tr4.Append(tr4tc1);

                        TableCell tr4tc2 = new TableCell();
                        tr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                        //tr4tc2.Append(new Paragraph(new Run(new Text(oProject.StartDate.ToString() + " - " + oProject.EndDate.ToString()))));
                        tr4tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM-d, yyyy}", oProject.StartDate) + " to " + String.Format("{0:MMMM-d, yyyy}", oProject.EndDate)))));

                        tr4.Append(tr4tc2);

                        table.Append(tr4);
                    }
                    //-----------------------------
                    //-----------Prime Partner Row
                    if (chkFieldPrimePartner.Checked == true)
                    {
                        TableRow tr5 = new TableRow();
                        TableCell tr5tc1 = new TableCell();
                        tr5tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                        tr5tc1.Append(new Paragraph(new Run(new Text("Prime Partner:"))));
                        tr5.Append(tr5tc1);

                        TableCell tr5tc2 = new TableCell();
                        tr5tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                        tr5tc2.Append(new Paragraph(new Run(new Text(oProject.PrimePartner))));
                        tr5.Append(tr5tc2);

                        table.Append(tr5);
                    }
                    //---------------Sectors-----------------------
                    if (chkSector.Checked == true)
                    {
                        TableRow tr6 = new TableRow();
                        TableCell tr6tc1 = new TableCell();
                        tr6tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                        tr6tc1.Append(new Paragraph(new Run(new Text("Sector:"))));
                        tr6.Append(tr6tc1);

                        TableCell tr6tc2 = new TableCell();
                        tr6tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                        tr6tc2.Append(new Paragraph(new Run(new Text(oProject.Sector))));
                        tr6.Append(tr6tc2);

                        table.Append(tr6);
                    }


                    //-----------Project Description Row
                    if (chkFieldProjectDescription.Checked == true)
                    {
                        TableRow tr7 = new TableRow();
                        TableCell tr7tc1 = new TableCell();
                        tr7tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                        tr7tc1.Append(new Paragraph(new Run(new Text("Project Description:"))));
                        tr7.Append(tr7tc1);

                        TableCell tr7tc2 = new TableCell();
                        tr7tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                        tr7tc2.Append(new Paragraph(new Run(new Text(oProject.ProjectDescription))));
                        tr7.Append(tr7tc2);

                        table.Append(tr7);
                    }
                    //----------------------------------------





                    body.Append(table);
                }
            }

            /////////////


        }
    }

    private void getTotalFunding()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oProjects2 = db.get_ProjectListByCountryPAreaAgency_X(sFilterCountry, sFilterPrimeParner, sFilterProgramArea, sFilterSubImplementingPartner, sFilterAgency, sFilterSector, sFilterStartFrom, sFilterStartTo, sFilterEndFrom, sFilterEndTo).ToList();
            object test2;
            res = 0;
            foreach (var t in oProjects2.ToList())
            {
                test2 = t.TotalFunding;
                res = Convert.ToInt64(test2) + res;
            }
        }
    }

    private void getCount()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oProjects1 = db.get_ProjectListByCountryPAreaAgency_X(sFilterCountry, sFilterPrimeParner, sFilterProgramArea, sFilterSubImplementingPartner, sFilterAgency, sFilterSector, sFilterStartFrom, sFilterStartTo, sFilterEndFrom, sFilterEndTo).ToList();
            test = oProjects1.ToList().Count();
        }
    }
    protected void genDocument()
    {
        //string sourceFile = @"E:\Work06\MEPIProject\Reports\Templates\ReportByCountry.docx";
        //string sTempFileName = @"E:\Work06\MEPIProject\Reports\Temp\ReportByCountry" + "_" + Guid.NewGuid().ToString() + ".docx";
        //// string destinationFile = Server.MapPath
        ////(Path.Combine("/", "../Temp/ReportByCountry_" + Guid.NewGuid().ToString() + ".docx"));


        string sourceFile = ConfigurationManager.AppSettings["SourceFilePath"].ToString();
        string sFileName = ConfigurationManager.AppSettings["SourceTempFileName"].ToString();


        string sTempFileName = sFileName + "_" + Guid.NewGuid().ToString() + ".docx";

        File.Copy(sourceFile, sTempFileName, true);
        WriteToWordDoc(sTempFileName);
        try
        {
            //Response.Write("<script>window.close();</script>")
            Response.Clear();
           // string sFilename = "Dos Report.docx";
            string sFilename = sFilterCountry + "_" + "Report.docx";
            //Response.ContentType = "application/msword";
            Response.ClearHeaders();

            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);

            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);

            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);
            }
        }        
            
    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        getFields();
        getFilters();
        ExportCustomReport(sFilterCountry, sFilterPrimeParner, sFilterProgramArea, sFilterSubImplementingPartner, sFilterAgency, sFilterSector, sFilterStartFrom, sFilterStartTo, sFilterEndFrom, sFilterEndTo);
    }

    private void ExportCustomReport(string sFilterCountry, string sFilterPrimeParner, string sFilterProgramArea, string sFilterSubImplementingPartner, string sFilterAgency, string sFilterSector, DateTime? sFilterStartFrom, DateTime? sFilterStartTo, DateTime? sFilterEndFrom, DateTime? sFilterEndTo)
    {
        DataSet ds= new DataSet();
        DataTable dt = new DataTable();

        //ds = rep1.GetCustomReportBySearch(sFilterCountry, sFilterPrimeParner, sFilterProgramArea, sFilterSubImplementingPartner, sFilterAgency, sFilterSector, sFilterStartFrom, sFilterStartTo, sFilterEndFrom, sFilterEndTo, "get_ProjectListByCountryPAreaAgency_X");
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var oProjects = db.get_ProjectListByCountryPAreaAgency_X(sFilterCountry, sFilterPrimeParner, sFilterProgramArea, sFilterSubImplementingPartner, sFilterAgency, sFilterSector, sFilterStartFrom, sFilterStartTo, sFilterEndFrom, sFilterEndTo);

            dt = oProjects.ToDataTable();           
            ds.Tables.Add(dt);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
            {
                ExportToExcel(ds, 0, Response, "Custom Report");
            }
            else
            {
                ExcelRepMessage.Text = "No Records Found.";
            }
        }


    }
    private void ExportToExcel(DataSet ds, int TableIndex, HttpResponse Response, string FileName)
    {
        int i, j;
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        // Response.ContentType = "application/vnd.ms-excel";
        string timestamp = Convert.ToString(DateTime.Now);
        timestamp = timestamp.Replace(" ", "_");
        timestamp = timestamp.Replace("/", "_");
        //  timestamp = timestamp.Replace(":", ":");
        FileName = FileName.Replace(" ", "");

        string ExtractName = "Projects" + "(" + timestamp + ")" + FileName + " .xlsx";
        //  Response.AppendHeader("content-disposition", "attachment; filename=" + ExtractName + ".xls");
        string sDest = @"E:\xx\SampleExcel\Docs\padp.xlsx";
        string sTempFileName = Server.MapPath(@"~\tmp\dataeX_") + Guid.NewGuid().ToString() + ".xlsx";

        ExportDataSet(ds, sTempFileName);
        displayExport(sTempFileName, ExtractName);

    }

    private void PrepareGridViewForExport(System.Web.UI.Control control)
    {
        string content = "";
        for (int i = control.Controls.Count - 1; i >= 0; i--)
        {
            PrepareGridViewForExport(control.Controls[i]);
        }
        if (!(control is TableCell))
        {
            if (control.GetType().GetProperty("SelectedItem") != null)
            {
                LiteralControl literal = new LiteralControl();
                control.Parent.Controls.Add(literal);
                try
                {
                    literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                }
                catch
                {
                }
                control.Parent.Controls.Remove(control);
            }
            else
                if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    // if (control.GetType() == typeof(CheckBox) || control.GetType() == typeof(RadioButton))
                    if (control.GetType() == typeof(CheckBox))
                    {
                        literal.Text = (bool)control.GetType().GetProperty("Checked").GetValue(control, null) ? "Yes" : "";
                    }
                    else
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);

                    control.Parent.Controls.Remove(control);
                }
        }
        return;
    }
    public static void ExportDataSet(DataSet ds, string destination)
    {
        using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
        {
            var workbookPart = workbook.AddWorkbookPart();

            workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

            foreach (System.Data.DataTable table in ds.Tables)
            {

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                uint sheetId = 1;
                if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                {
                    sheetId =
                        sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }

                DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }


                sheetData.AppendChild(headerRow);

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (String col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        dynamic dc = dsrow[col];
                        if (dc.GetType().ToString() == "System.Boolean")
                        {
                            if (dsrow[col].ToString() == "True")
                            {
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Yes");
                            }

                        }
                        else
                        {
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString());
                        }
                        //if (Type.GetType(dc.GetType().ToString()))
                        //
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

            }
        }
    }
    private void displayExport(string sTempFileName, string sFilename)
    {
        try
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.spreadsheet";
            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);
            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);

            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);

            }
        }
    }

    protected void chkCountry_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCountry.Checked == true)
        {
            ddlCountry.Enabled = true;
            ExcelRepMessage.Text = "";
        }
        else
        {
            ddlCountry.Enabled = false;
            ddlCountry.SelectedIndex = 0;
            ExcelRepMessage.Text = "";
        }
    }
    protected void chkAgency_CheckedChanged(object sender, EventArgs e)
    {

        if (chkAgency.Checked == true)
        {
            cboAgency.Enabled = true;
            ExcelRepMessage.Text = "";
        }
        else
        {
            cboAgency.Enabled = false;
            cboAgency.SelectedIndex = 0;
            ExcelRepMessage.Text = "";
        }
    }

    protected void chkProgramArea_CheckedChanged(object sender, EventArgs e)
    {
        if (chkProgramArea.Checked == true)
        {
            cboProgramAreas.Enabled = true;
            ExcelRepMessage.Text = "";
        }
        else
        {
            cboProgramAreas.Enabled = false;
            cboProgramAreas.SelectedIndex = 0;
            ExcelRepMessage.Text = "";
        }
    }
    protected void chkSector_CheckedChanged(object sender, EventArgs e)
    {
        if (chkSector.Checked == true)
        {
            cboSector.Enabled = true;
            ExcelRepMessage.Text = "";
        }
        else
        {
            cboSector.Enabled = false;
            cboSector.SelectedIndex = 0;
            ExcelRepMessage.Text = "";
        }
    }
    protected void chkPrimeParner_CheckedChanged(object sender, EventArgs e)
    {
        if (chkPrimeParner.Checked == true)
        {
            txtPrimePartner.Enabled = true;
            ExcelRepMessage.Text = "";
        }
        else
        {
            txtPrimePartner.Enabled = false;
            txtPrimePartner.Text= null;
            ExcelRepMessage.Text = "";
        }
    }
    protected void chkSubImplementingPartner_CheckedChanged(object sender, EventArgs e)
    {
        if (chkSubImplementingPartner.Checked == true)
        {
            txtSubImplementingPartner.Enabled = true;
            ExcelRepMessage.Text = "";
        }
        else
        {
            txtSubImplementingPartner.Enabled = false;
            txtSubImplementingPartner.Text = null;
            ExcelRepMessage.Text = "";
        }
    }
    protected void chkStartDate_CheckedChanged(object sender, EventArgs e)
    {
        if (chkStartDate.Checked == true)
        {
            txtStartDateFrom.Enabled = true;
            txtStartDateTo.Enabled = true;
            ExcelRepMessage.Text = "";
        }
        else
        {
            txtStartDateFrom.Enabled = false;
            txtStartDateTo.Enabled = false;
            txtStartDateFrom.Text = null;
            txtStartDateTo.Text = null;
            ExcelRepMessage.Text = "";
        }
    }
    protected void chkEndDate_CheckedChanged(object sender, EventArgs e)
    {
        if (chkEndDate.Checked == true)
        {
            txtEndDateFrom.Enabled = true;
            txtEndDateTo.Enabled = true;
            ExcelRepMessage.Text = "";
        }
        else
        {
            txtEndDateFrom.Enabled = false;
            txtEndDateTo.Enabled = false;
            txtEndDateFrom.Text = null;
            txtEndDateTo.Text = null;
            ExcelRepMessage.Text = "";
        }
    }
    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        chkCountry.Checked = false;
        ddlCountry.Enabled = false;
        ddlCountry.SelectedIndex = -1;


        chkAgency.Checked = false;
        cboAgency.Enabled = false;
        cboAgency.SelectedIndex = -1;

        chkProgramArea.Checked = false;
        cboProgramAreas.Enabled = false;
        cboProgramAreas.SelectedIndex = -1;

        chkSector.Checked = false;
        cboSector.Enabled = false;
        cboSector.SelectedIndex = -1;

        chkPrimeParner.Checked = false;
        txtPrimePartner.Enabled = false;
        txtPrimePartner.Text = null;

        chkStartDate.Checked = false;
        txtStartDateFrom.Enabled = false;
        txtStartDateTo.Enabled = false;
        txtStartDateFrom.Text = null;
        txtStartDateTo.Text = null;


        chkEndDate.Checked = false;
        txtEndDateFrom.Enabled = false;
        txtEndDateTo.Enabled = false;
        txtEndDateFrom.Text = null;
        txtEndDateTo.Text = null;

        ExcelRepMessage.Text = "";
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void cboAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void cboProgramAreas_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void cboSector_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void txtPrimePartner_TextChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void txtSubImplementingPartner_TextChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void txtStartDateFrom_TextChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void txtStartDateTo_TextChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void txtEndDateTo_TextChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void txtEndDateFrom_TextChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
}
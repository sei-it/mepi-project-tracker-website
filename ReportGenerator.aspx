﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportGenerator.aspx.cs" Inherits="ReportGenerator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h2>Choose Fields</h2>
        <p>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatColumns="3">
                <asp:ListItem>Agency</asp:ListItem>
                <asp:ListItem>Prime Parner</asp:ListItem>
                <asp:ListItem>Project Name</asp:ListItem>
                <asp:ListItem>Project Description</asp:ListItem>
                <asp:ListItem>Countries</asp:ListItem>
                <asp:ListItem>Total Funding</asp:ListItem>
                <asp:ListItem>Funding Account</asp:ListItem>
                <asp:ListItem>Program  Area</asp:ListItem>
                <asp:ListItem>Unliquidated Obligation</asp:ListItem>
                <asp:ListItem>Most Recent Obligation</asp:ListItem>
                <asp:ListItem>FY of Most Recent Obligation</asp:ListItem>
                <asp:ListItem>Start Date</asp:ListItem>
                <asp:ListItem>End Date</asp:ListItem>
                <asp:ListItem>Sector</asp:ListItem>
            </asp:CheckBoxList>
        </p>
    
    </div>
        <h2>Order By</h2>
        <p>Option 1</p>
        <asp:dropdownlist ID="CheckBoxList2" runat="server">
            <asp:ListItem>Agency</asp:ListItem>
            <asp:ListItem>Prime Parner</asp:ListItem>
            <asp:ListItem>Project Name</asp:ListItem>
            <asp:ListItem>Project Description</asp:ListItem>
            <asp:ListItem>Countries</asp:ListItem>
            <asp:ListItem>Total Funding</asp:ListItem>
            <asp:ListItem>Funding Account</asp:ListItem>
            <asp:ListItem>Program  Area</asp:ListItem>
            <asp:ListItem>Unliquidated Obligation</asp:ListItem>
            <asp:ListItem>Most Recent Obligation</asp:ListItem>
            <asp:ListItem>FY of Most Recent Obligation</asp:ListItem>
            <asp:ListItem>Start Date</asp:ListItem>
            <asp:ListItem>End Date</asp:ListItem>
            <asp:ListItem>Sector</asp:ListItem>
        </asp:dropdownlist>
        <p>Option 2</p>
        <asp:dropdownlist ID="Dropdownlist1" runat="server">
            <asp:ListItem>Agency</asp:ListItem>
            <asp:ListItem>Prime Parner</asp:ListItem>
            <asp:ListItem>Project Name</asp:ListItem>
            <asp:ListItem>Project Description</asp:ListItem>
            <asp:ListItem>Countries</asp:ListItem>
            <asp:ListItem>Total Funding</asp:ListItem>
            <asp:ListItem>Funding Account</asp:ListItem>
            <asp:ListItem>Program  Area</asp:ListItem>
            <asp:ListItem>Unliquidated Obligation</asp:ListItem>
            <asp:ListItem>Most Recent Obligation</asp:ListItem>
            <asp:ListItem>FY of Most Recent Obligation</asp:ListItem>
            <asp:ListItem>Start Date</asp:ListItem>
            <asp:ListItem>End Date</asp:ListItem>
            <asp:ListItem>Sector</asp:ListItem>
        </asp:dropdownlist>
        <p>Option 3</p>
        <asp:dropdownlist ID="Dropdownlist2" runat="server">
            <asp:ListItem>Agency</asp:ListItem>
            <asp:ListItem>Prime Parner</asp:ListItem>
            <asp:ListItem>Project Name</asp:ListItem>
            <asp:ListItem>Project Description</asp:ListItem>
            <asp:ListItem>Countries</asp:ListItem>
            <asp:ListItem>Total Funding</asp:ListItem>
            <asp:ListItem>Funding Account</asp:ListItem>
            <asp:ListItem>Program  Area</asp:ListItem>
            <asp:ListItem>Unliquidated Obligation</asp:ListItem>
            <asp:ListItem>Most Recent Obligation</asp:ListItem>
            <asp:ListItem>FY of Most Recent Obligation</asp:ListItem>
            <asp:ListItem>Start Date</asp:ListItem>
            <asp:ListItem>End Date</asp:ListItem>
            <asp:ListItem>Sector</asp:ListItem>
        </asp:dropdownlist>
        <h2>Filters</h2>
            <asp:CheckBoxList ID="CheckBoxList3" runat="server" RepeatColumns="1">
                <asp:ListItem>Agency</asp:ListItem>
                <asp:ListItem>Prime Parner</asp:ListItem>
                <asp:ListItem>Project Name</asp:ListItem>
                <asp:ListItem>Project Description</asp:ListItem>
                <asp:ListItem>Countries</asp:ListItem>
                <asp:ListItem>Total Funding</asp:ListItem>
                <asp:ListItem>Funding Account</asp:ListItem>
                <asp:ListItem>Program  Area</asp:ListItem>
                <asp:ListItem>Unliquidated Obligation</asp:ListItem>
                <asp:ListItem>Most Recent Obligation</asp:ListItem>
                <asp:ListItem>FY of Most Recent Obligation</asp:ListItem>
                <asp:ListItem>Start Date</asp:ListItem>
                <asp:ListItem>End Date</asp:ListItem>
                <asp:ListItem>Sector</asp:ListItem>
            </asp:CheckBoxList>
    </form>
    <p>
        &nbsp;</p>
</body>
</html>

var simplemaps_worldmap_mapdata={

main_settings:{ 
		width: '1200',
		background_color: '#FFFFFF',
		background_transparent: 'yes',
		border_color: '#FFFFFF',
		pop_ups: 'on_click',
		location_popups: 'on_click',
		state_description: 'Excel Report  |  DOC Report',
		state_color: '#88A4BC',
		state_hover_color: '#3B729F',
		state_url: '',
		all_states_inactive: 'no',
		location_url: '',
		location_description: '',
		location_color: 'green',
		location_size: '30',
		location_type: 'circle',
		all_locations_inactive: 'no',
		location_type: 'square',
		location_opacity: '1',
		initial_zoom: '5',
		initial_zoom_solo: 'yes',
		zoom: 'no',
		hide_label: 'no',
		lable_color: 'green'
},

state_specific:{ 
	"AE": { 
		name: 'United Arab Emirates',
		description: '<a href="Default.aspx?sState=United Arab Emirates&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=United Arab Emirates&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(126, 161, 205)',
		hover_color: 'default',
		inactive: 'no',
		url: 'default',
		hide: 'no'
	},
	"AF": { 
		name: 'Afghanistan',
		description: '<a href="Default.aspx?sState=YAfghanistan&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Afghanistan&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"AL": { 
		name: 'Albania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"AM": { 
		name: 'Armenia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"AO": { 
		name: 'Angola',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"AR": { 
		name: 'Argentina',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"AT": { 
		name: 'Austria',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"AU": { 
		name: 'Australia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"AZ": { 
		name: 'Azerbaidjan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BA": { 
		name: 'Bosnia-Herzegovina',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BD": { 
		name: 'Bangladesh',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BE": { 
		name: 'Belgium',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BF": { 
		name: 'Burkina Faso',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BG": { 
		name: 'Bulgaria',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BH": { 
		name: 'Bahrain',
		description: '<a href="Default.aspx?sState=Bahrain&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Bahrain&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BI": { 
		name: 'Burundi',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BJ": { 
		name: 'Benin',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BN": { 
		name: 'Brunei Darussalam',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BO": { 
		name: 'Bolivia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BR": { 
		name: 'Brazil',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BS": { 
		name: 'Bahamas',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BT": { 
		name: 'Bhutan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BW": { 
		name: 'Botswana',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BY": { 
		name: 'Belarus',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"BZ": { 
		name: 'Belize',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CA": { 
		name: 'Canada',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CD": { 
		name: 'Congo',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CF": { 
		name: 'Central African Republic',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CG": { 
		name: 'Congo',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CH": { 
		name: 'Switzerland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CI": { 
		name: 'Ivory Coast',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CL": { 
		name: 'Chile',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CM": { 
		name: 'Cameroon',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CN": { 
		name: 'China',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CO": { 
		name: 'Colombia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CR": { 
		name: 'Costa Rica',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CU": { 
		name: 'Cuba',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CV": { 
		name: 'Cape Verde',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CY": { 
		name: 'Cyprus',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"CZ": { 
		name: 'Czech Republic',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"DE": { 
		name: 'Germany',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"DJ": { 
		name: 'Djibouti',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"DK": { 
		name: 'Denmark',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"DO": { 
		name: 'Dominican Republic',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"DZ": { 
		name: 'Algeria',
		description: '<a href="Default.aspx?sState=Algeria&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Algeria&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(126, 161, 205)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"EC": { 
		name: 'Ecuador',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"EE": { 
		name: 'Estonia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"EG": { 
		name: 'Egypt',
		description: '<a href="Default.aspx?sState=Egypt&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Egypt&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(153, 145, 119)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"EH": { 
		name: 'Western Sahara',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ER": { 
		name: 'Eritrea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ES": { 
		name: 'Spain',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ET": { 
		name: 'Ethiopia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"FI": { 
		name: 'Finland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"FJ": { 
		name: 'Fiji',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"FK": { 
		name: 'Falkland Islands',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"FR": { 
		name: 'France',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GA": { 
		name: 'Gabon',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GB": { 
		name: 'Great Britain',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GE": { 
		name: 'Georgia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GF": { 
		name: 'French Guyana',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GH": { 
		name: 'Ghana',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GL": { 
		name: 'Greenland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GM": { 
		name: 'Gambia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GN": { 
		name: 'Guinea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GQ": { 
		name: 'Equatorial Guinea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GR": { 
		name: 'Greece',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GS": { 
		name: 'S. Georgia & S. Sandwich Isls.',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GT": { 
		name: 'Guatemala',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GW": { 
		name: 'Guinea Bissau',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"GY": { 
		name: 'Guyana',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"HN": { 
		name: 'Honduras',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"HR": { 
		name: 'Croatia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"HT": { 
		name: 'Haiti',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"HU": { 
		name: 'Hungary',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"IC": { 
		name: 'Canary Islands',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ID": { 
		name: 'Indonesia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"IE": { 
		name: 'Ireland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"IL": { 
		name: 'Israel',
		description: '<a href="Default.aspx?sState=Israel&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Israel&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(161, 207, 165)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"IN": { 
		name: 'India',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"IQ": { 
		name: 'Iraq',
		description: '<a href="Default.aspx?sState=Iraq&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Iraq&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(134, 189, 143)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"IR": { 
		name: 'Iran',
		description: '<a href="Default.aspx?sState=Iran&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Iran&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(153, 145, 119)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"IS": { 
		name: 'Iceland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"IT": { 
		name: 'Italy',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"JM": { 
		name: 'Jamaica',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"JO": { 
		name: 'Jordan',
		description: '<a href="Default.aspx?sState=Jordan&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Jordan&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(126, 161, 205)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"JP": { 
		name: 'Japan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"KE": { 
		name: 'Kenya',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"KG": { 
		name: 'Kyrgyzstan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"KH": { 
		name: 'Cambodia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"KP": { 
		name: 'North Korea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"KR": { 
		name: 'South Korea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"KW": { 
		name: 'Kuwait',
		description: '<a href="Default.aspx?sState=Kuwait&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Kuwait&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(126, 161, 205)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"KZ": { 
		name: 'Kazakhstan',
		description: '<a href="Default.aspx?sState=Yemen&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Yemen&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LA": { 
		name: 'Laos',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LB": { 
		name: 'Lebanon',
		description: '<a href="Default.aspx?sState=Lebanon&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Lebanon&sRT=DOC" class="doc">WORD Report</a>',
		color: 'rgb(202, 188, 145)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LK": { 
		name: 'Sri Lanka',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LR": { 
		name: 'Liberia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LS": { 
		name: 'Lesotho',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LT": { 
		name: 'Lithuania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LU": { 
		name: 'Luxembourg',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LV": { 
		name: 'Latvia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"LY": { 
		name: 'Libya',
		description: '<a href="Default.aspx?sState=Libya&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Libya&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(134, 189, 143)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MA": { 
		name: 'Morocco',
		description: '<a href="Default.aspx?sState=Morocco&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Morocco&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(153, 145, 119)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MD": { 
		name: 'Moldavia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ME": { 
		name: 'Montenegro',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MG": { 
		name: 'Madagascar',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MK": { 
		name: 'Macedonia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ML": { 
		name: 'Mali',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MM": { 
		name: 'Myanmar',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MN": { 
		name: 'Mongolia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MR": { 
		name: 'Mauritania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MW": { 
		name: 'Malawi',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MX": { 
		name: 'Mexico',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MY": { 
		name: 'Malaysia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"MZ": { 
		name: 'Mozambique',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NA": { 
		name: 'Namibia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NC": { 
		name: 'New Caledonia (French)',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NE": { 
		name: 'Niger',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NG": { 
		name: 'Nigeria',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NI": { 
		name: 'Nicaragua',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NL": { 
		name: 'Netherlands',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NO": { 
		name: 'Norway',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NP": { 
		name: 'Nepal',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"NZ": { 
		name: 'New Zealand',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"OM": { 
		name: 'Oman',
		description: '<a href="Default.aspx?sState=Oman&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Oman&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(134, 189, 143)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PA": { 
		name: 'Panama',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PE": { 
		name: 'Peru',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PG": { 
		name: 'Papua New Guinea',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PH": { 
		name: 'Philippines',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PK": { 
		name: 'Pakistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PL": { 
		name: 'Poland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PR": { 
		name: 'Puerto Rico',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PS": { 
		name: 'Palestine',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PT": { 
		name: 'Portugal',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"PY": { 
		name: 'Paraguay',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"QA": { 
		name: 'Qatar',
	    //description: 'default',
		description: '<a href="Default.aspx?sState=Qatar&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Qatar&sRT=DOC" class="doc">WORD Report</a>',
		color: 'rgb(153, 145, 119)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"RO": { 
		name: 'Romania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"RS": { 
		name: 'Serbia',
		description: '<a href="Default.aspx?sState=Serbia&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Serbia&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"RU": { 
		name: 'Russia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"RW": { 
		name: 'Rwanda',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SA": { 
		name: 'Saudi Arabia',
		description: '<a href="Default.aspx?sState=Saudi Arabia&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Saudi Arabia&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(202, 188, 145)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SB": { 
		name: 'Solomon Islands',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SD": { 
		name: 'Sudan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SE": { 
		name: 'Sweden',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SI": { 
		name: 'Slovenia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SK": { 
		name: 'Slovak Republic',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SL": { 
		name: 'Sierra Leone',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SN": { 
		name: 'Senegal',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SO": { 
		name: 'Somalia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SR": { 
		name: 'Suriname',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SS": { 
		name: 'South Sudan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SV": { 
		name: 'El Salvador',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SY": { 
		name: 'Syria',
		description: '<a href="Default.aspx?sState=Syria&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Syria&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(153, 145, 119)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"SZ": { 
		name: 'Swaziland',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TD": { 
		name: 'Chad',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TG": { 
		name: 'Togo',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TH": { 
		name: 'Thailand',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TJ": { 
		name: 'Tadjikistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TL": { 
		name: 'East Timor',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TM": { 
		name: 'Turkmenistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TN": { 
		name: 'Tunisia',
		description: '<a href="Default.aspx?sState=Tunisia&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Tunisia&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(202, 188, 145)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TR": { 
		name: 'Turkey',
		description: '<a href="Default.aspx?sState=Turkey&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Turkey&sRT=DOC" class="doc">WORD Report</a>', 
		color: 'rgb(126, 161, 205)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TT": { 
		name: 'Trinidad and Tobago',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TW": { 
		name: 'Taiwan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"TZ": { 
		name: 'Tanzania',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"UA": { 
		name: 'Ukraine',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"UG": { 
		name: 'Uganda',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"US": { 
		name: 'United States',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"UY": { 
		name: 'Uruguay',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"UZ": { 
		name: 'Uzbekistan',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"VE": { 
		name: 'Venezuela',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"VN": { 
		name: 'Vietnam',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"VU": { 
		name: 'Vanuatu',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"YE": { 
		name: 'Yemen',
	    //description: 'default',
		description: '<a href="Default.aspx?sState=Yemen&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Yemen&sRT=DOC" class="doc">WORD Report</a>',       
		color: 'rgb(153, 145, 119)',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ZA": { 
		name: 'South Africa',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ZM": { 
		name: 'Zambia',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	},
	"ZW": { 
		name: 'Zimbabwe',
		description: 'default',
		color: 'default',
		hover_color: 'default',
		url: 'default',
		hide: 'no',
		inactive: 'no'
	}
},

locations:{ 
	"0": { 
		name: 'Bahrain',
		lat: '26.0275',
		lng: '50.5500',
		color: 'default',
		pop_ups: 'on_click',
		description: '<a href="Default.aspx?sState=Bahrain&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Bahrain&sRT=DOC" class="doc">WORD Report</a>', 
		url: '',
		state_url: '',
		size: '22',
		type: 'square',
		opacity: '1'
	},
	"1": { 
		name: 'West Bank/Gaza',
		lat: '31.041266',
		lng: '34.862176',
		color: 'default',
		pop_ups: 'on_click',
		description: '<a href="Default.aspx?sState=West Bank/Gaza&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=West Bank/Gaza&sRT=DOC" class="doc">WORD Report</a>', 
		url: '',
		state_url: '',
		size: '22',
		type: 'square',
		opacity: '1'
	},
	"2": { 
		name: 'Regional',
		lat: '17.041266',
		lng: '-8.862176',
		pop_ups: 'on_click',
		color: 'default',
		description: '<a href="Default.aspx?sState=Regional&sRT=Excel" class="excel">EXCEL Report</a> | <a href="Default.aspx?sState=Regional&sRT=DOC" class="doc">WORD Report</a>',
		url: 'default',
		size: '30',
		type: 'square',
		opacity: '1'
	}
},

regions:{ 
	"0": { 
		name: 'North America',
		states:["MX","CA","US","GL"]
	},
	"1": { 
		name: 'South America',
		states:["EC","AR","VE","BR","CO","BO","PE","CL","GT","GY","GF","PY","SR","UY"]
	},
	"2": { 
		name: 'Central America',
		states:["PR","JM","HT","BZ","CR","DO","GT","HN","NI","BS","CU","PA","SV"]
	},
	"3": { 
		name: 'Europe',
		states:["IT","NL","NO","DK","IE","GB","RO","DE","FR","AL","AM","AT","BY","BE","LU","BG","CZ","EE","GE","GR","HU","IS","LV","LT","MD","PL","PT","RS","SI","HR","BA","ME","MK","SK","ES","FI","SE","CH","CY","UA"]
	},
	"4": { 
		name: 'Africa',
		states:["ZW"]
	},
	"5": { 
		name: 'MEPI',
		states:["DZ","EG","IR","IQ","IL","JO","KW","LB","LY","MA","OM","QA","TR","AE","SA","SY","TN","YE"]
	},
	"6": { 
		name: 'South Asia',
		states:["TW","IN","NP","TH","BN","JP","VN","LK","SB","BD","BT","KH","LA","MM","NP","KP","PH","KR","CN"]
	},
	"7": { 
		name: 'Oceania',
		states:["ID","AU","MY","PG","FJ","NZ"]
	},
	"8": { 
		name: 'North Asia<br /> Hi There',
		states:["MN","RU","KZ"]
	}
},

labels: {
		'DZ':{parent_id: 'DZ', name: 'Algeria', x: '990', y: '388', color: '#000000'},
		'EG':{parent_id: 'EG', name: 'Egypt', x: '1138', y: '405', color: '#000000'},
		'IR':{parent_id: 'IR', name: 'Iran', x: '1262', y: '364', color: '#000000'},
		'IQ':{parent_id: 'IQ', name: 'Iraq', x: '1206', y: '361', color: '#000000'},
		'IL':{parent_id: 'IL', name: 'IL', x: '1160', y: '364', line: 'yes', hover_color:'green', color: '#000000'},
		'JO':{parent_id: 'JO', name: 'JO', x: '1173', y: '378', color: '#000000'},
		'KW':{parent_id: 'KW', name: 'KW', x: '1243', y: '386', hover_color:'green', color: '#000000'},
		'LB':{parent_id: 'LB', name: 'LB', x: '1161', y: '355', hover_color:'green', color: '#000000', line: 'yes', display: 'region'},
		'LY':{parent_id: 'LY', name: 'Libya', x: '1073', y: '400', color: '#000000'},
		'MA':{parent_id: 'MA', name: 'Morocco', x: '946.36', y: '364.32', color: '#000000'},
		'OM':{parent_id: 'OM', name: 'Oman', x: '1293', y: '432', color: '#000000'},
		'QA':{parent_id: 'QA', name: 'QA', x: '1264', y: '412', hover_color:'green', color: '#000000'},
		'TR':{parent_id: 'TR', name: 'Turkey', x: '1161', y: '322', color: '#000000'},
		'AE':{parent_id: 'AE', name: 'AE', x: '1274', y: '423.08', color: '#000000'},
		'SA':{parent_id: 'SA', name: 'Saudi Arabia', x: '1223', y: '419', color: '#000000', line: 'yes', popup_width: '1111'},
		'SY':{parent_id: 'SY', name: 'Syria', x: '1181', y: '350', color: '#000000'},
		'TN':{parent_id: 'TN', name: 'Tunisia', x: '1029.333', y: '357.447', color: '#000000'},
		'YE':{parent_id: 'YE', name: 'Yemen', x: '1243', y: '472', color: '#000000'},
		'0':{parent_type: 'location', parent_id: '0', line: 'yes', name: 'BH', x: '1253', y: '399', color: '#000000', hover_color:'green', display: 'region', line: 'yes', type: 'circle', location_size: '10'},
		'1':{parent_type: 'location', parent_id: '1', name: 'WBG', x: '1140', y: '367', color: '#000000', display: 'region', hover_color:'green', line: 'yes', pill: 'yes', type: 'circle'},
		'2':{parent_type: 'location', parent_id: '2', name: 'Regional', x: '950', y: '463.5', color: '#000000', hover_color:'green', display: 'region', pill: 'yes', line: 'yes'}		
		
	}


}
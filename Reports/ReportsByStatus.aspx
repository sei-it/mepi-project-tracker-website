﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReportsByStatus.aspx.cs" Inherits="Reports_ReportsByStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <h2>
        Projects By Status
    </h2>

    <p>
        Filter by Status
        <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"></asp:DropDownList>
    </p>

    <p>
        <asp:Button ID="btnPrint" runat="server" Text="Report In Word" OnClick="btnPrint_Click" />
        <asp:Button ID="btnExcel" runat="server" Text="Report In Excel" OnClick="btnExcel_Click" />
          <asp:Button ID="btnClearAll" runat="server" Text="Clear all" OnClick="btnClearAll_Click" />
    </p>
     <p><asp:Label runat="server" ID="ExcelRepMessage" ForeColor="Red"></asp:Label></p>

</asp:Content>


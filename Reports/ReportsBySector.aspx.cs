﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.IO;
using DocumentFormat.OpenXml;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

public partial class Reports_ReportsBySector : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["MEPI_ProjectsConnectionString"].ToString();
    string sSector = "";
    string sProgrameArea = "";


    DataSet dsInc = new DataSet();
   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState["IsLoaded1"] == null)
        {
            loadDropdown();
        }
        ViewState["IsLoaded1"] = true;
        Page.MaintainScrollPositionOnPostBack = true;
       
    }

    private void loadDropdown()
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.Sectors
                      orderby e.Sector1
                      select e;
            ddlSectors.DataSource = qry;
            ddlSectors.DataTextField = "Sector1";
            ddlSectors.DataValueField = "SectorID";
            ddlSectors.DataBind();
            ddlSectors.Items.Insert(0, "");
       

        var qry5 = from e in db.ProgramAreas                   
                   orderby e.ProgramArea1
                   select e;
        dllFprograme.DataSource = qry5;
        dllFprograme.DataTextField = "ProgramArea1";
        dllFprograme.DataValueField = "ProgramAreaID";
        dllFprograme.DataBind();
        dllFprograme.Items.Insert(0, "");

        }
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
       

        if (!(ddlSectors.SelectedItem == null))
        {
            if (!(ddlSectors.SelectedItem.Text.ToString() == ""))
            {
                sSector = ddlSectors.SelectedItem.Text;
            }
        }

        if (!(dllFprograme.SelectedItem == null))
        {
            if (!(dllFprograme.SelectedItem.Text.ToString() == ""))
            {
                sProgrameArea = dllFprograme.SelectedItem.Text;
            }
        }


        string sourceFile = ConfigurationManager.AppSettings["SourceFilePath"].ToString();
        string sFileName = ConfigurationManager.AppSettings["SourceTempFileName"].ToString();


        string sTempFileName = sFileName + "_" + Guid.NewGuid().ToString() + ".docx";


        // string sTempFileName = Server.MapPath
        // (Path.Combine("/", "../Temp/ReportByCountry_" + Guid.NewGuid().ToString() + ".docx"));

        File.Copy(sourceFile, sTempFileName, true);
        WriteToWordDoc(sTempFileName, sSector, sProgrameArea);
        try
        {
            Response.Clear();
            string sFilename = sSector + "_" + "Report.docx";
            //Response.ContentType = "application/msword";
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);
            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);
            Response.End();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);
            }
        }        
            
 
    }
    public static void WriteToWordDoc(string filepath, string sSector, string sProgrameArea)
    {
        string sTmpCountry = "";
        string sTmpProjectName = "";
        string sTmpProgramArea = "";
        string sTmpSectors = "";
        string sTmpFundingAccount = "";
        // Open a WordprocessingDocument for editing using the filepath.
        using (WordprocessingDocument wordprocessingDocument =
             WordprocessingDocument.Open(filepath, true))
        {
            // Assign a reference to the existing document body.
            Body body = wordprocessingDocument.MainDocumentPart.Document.Body;


           
 
            /////////////////////////////////////////////////////////
            /////////////
            using (DataClasses2DataContext db = new DataClasses2DataContext())
            {
              

                //Project Information

               

                Paragraph wrdDate = new Paragraph(
                new ParagraphProperties(
                    new ParagraphStyleId() { Val = "Heading4" }),
                new Run(new Text(" Report Date: " + String.Format("{0:MMMM-d, yyyy}", DateTime.Now))));
                body.Append(wrdDate);


                //---- count
                var oProjects1 = db.get_ProjectListBySectors(sSector, sProgrameArea);
                int test = oProjects1.Count();

                Paragraph wrdCount = new Paragraph(
                new ParagraphProperties(
                new ParagraphStyleId() { Val = "Heading4" }),
                new Run(new Text(" Project Count: " + test)));
                body.Append(wrdCount);

                //---Sum Total funding

               var oProjects2 = db.get_ProjectListBySectors(sSector, sProgrameArea);
                object test2;
                Int64 res = 0;
                foreach (var t in oProjects2.ToList())
                {
                    test2 = t.TotalFunding;
                    res = Convert.ToInt64(test2) + res;
                }

                Paragraph wrdSumTFunding = new Paragraph(
               new ParagraphProperties(
                   new ParagraphStyleId() { Val = "Heading4" }),
               new Run(new Text("Total Funding: " + String.Format("{0:C}", res))));
                body.Append(wrdSumTFunding);

                //--------------

                Paragraph wrdParaCoutry = new Paragraph(
                            new ParagraphProperties(
                                new ParagraphStyleId() { Val = "Title" }),
                            new Run(
                                new Text("Projects By Sector")));
                body.Append(wrdParaCoutry);

                var oProjects = db.get_ProjectListBySectors(sSector, sProgrameArea);
                foreach(get_ProjectListBySectorsResult oProject in oProjects)
                {


                    //if (sTmpSectors != oProject.Sectors)
                    //{
                    //    sTmpSectors = oProject.Sectors;


                    //    Paragraph wrdParaCoutry = new Paragraph(
                    //        new ParagraphProperties(
                    //            new ParagraphStyleId() { Val = "Title" }),
                    //        new Run(
                    //            new Text("Projects By Sector")));
                    //    body.Append(wrdParaCoutry);
                    //    /////////////////////////////////////////////////////////
                    //}




                    if (sTmpSectors != sSector)
                    {
                        sTmpSectors = sSector;

                        Paragraph newParaProgramArea = new Paragraph(
                            new ParagraphProperties(
                                new ParagraphStyleId() { Val = "Heading1" }),
                            new Run(
                                new Text("Sector: " + sTmpSectors)));
                        body.Append(newParaProgramArea);
                        /////////////////////////////////////////////////////////
                    }



                    if (sTmpProgramArea != oProject.ProgramArea)
                    {
                        sTmpProgramArea = oProject.ProgramArea;

                        Paragraph newParaProgramArea = new Paragraph(
                            new ParagraphProperties(
                                new ParagraphStyleId() { Val = "Heading2" }),
                            new Run(
                                new Text("Program Area: " + sTmpProgramArea)));
                        body.Append(newParaProgramArea);
                        /////////////////////////////////////////////////////////
                    }
                   

                    //Project Information
                    Paragraph wrdParaProject = new Paragraph(
                    new ParagraphProperties(
                        new ParagraphStyleId() { Val = "Heading3" }),
                    new Run(
                        new Text(oProject.ProjectName)));
                    body.Append(wrdParaProject);


                    Table table = new Table();
                    

                    TableProperties tblPr = new TableProperties();
                    TableStyle tblStyle = new TableStyle();
                    tblStyle.Val = "LightShading-Accent1";
                    tblPr.AppendChild(tblStyle);
                    table.AppendChild(tblPr);


                    //-----------First Row
                    TableRow tr = new TableRow();
                    TableCell tc1 = new TableCell();
                    tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                    tc1.Append(new Paragraph(new Run(new Text("USG Agency/Office: "))));
                    tr.Append(tc1);

                    TableCell tc2 = new TableCell();
                    tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                    tc2.Append(new Paragraph(new Run(new Text(oProject.AgencyAbbr))));
                    tr.Append(tc2);
                    table.Append(tr);

                    //-----------Second Row
                    TableRow tr2 = new TableRow();
                    TableCell tr2tc1 = new TableCell();
                    tr2tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                    tr2tc1.Append(new Paragraph(new Run(new Text("Total Funding:"))));
                    tr2.Append(tr2tc1);

                    TableCell tr2tc2 = new TableCell();
                    tr2tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                    tr2tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:C}", oProject.TotalFunding)))));
                    tr2.Append(tr2tc2);

                    table.Append(tr2);
                    //-----------------------------
                    //-----------Funding Account Row
                    TableRow tr3 = new TableRow();
                    TableCell tr3tc1 = new TableCell();
                    tr3tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                    tr3tc1.Append(new Paragraph(new Run(new Text("Funding Account:"))));
                    tr3.Append(tr3tc1);

                    TableCell tr3tc2 = new TableCell();
                    tr3tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));

                    //tr3tc2.Append(new Paragraph(new Run(new Text(oProject.FundingAccount.ToString())))); //commented by on Feb 3 2015
                    //tr3.Append(tr3tc2);


                    if (oProject.FundingAccount !=null)
                    {
                        tr3tc2.Append(new Paragraph(new Run(new Text(oProject.FundingAccount.ToString()))));
                        tr3.Append(tr3tc2);
                    }
                    else
                    {
                        tr3tc2.Append(new Paragraph(new Run(new Text(""))));
                        tr3.Append(tr3tc2);

                    }

                    table.Append(tr3);
                    //-----------------------------
                    //-----------Funding Account Row
                    TableRow tr4 = new TableRow();
                    TableCell tr4tc1 = new TableCell();
                    tr4tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                    tr4tc1.Append(new Paragraph(new Run(new Text("Project Timeline:"))));
                    tr4.Append(tr4tc1);

                    TableCell tr4tc2 = new TableCell();
                    tr4tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                    //tr4tc2.Append(new Paragraph(new Run(new Text(oProject.StartDate.ToString() + " - " + oProject.EndDate.ToString()))));
                    tr4tc2.Append(new Paragraph(new Run(new Text(String.Format("{0:MMMM-d, yyyy}", oProject.StartDate) + " to " + String.Format("{0:MMMM-d, yyyy}", oProject.EndDate)))));

                    tr4.Append(tr4tc2);

                    table.Append(tr4);
                    //-----------------------------
                    //-----------Funding Account Row
                    TableRow tr5 = new TableRow();
                    TableCell tr5tc1 = new TableCell();
                    tr5tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                    tr5tc1.Append(new Paragraph(new Run(new Text("Prime Partner:"))));
                    tr5.Append(tr5tc1);

                    TableCell tr5tc2 = new TableCell();
                    tr5tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                    tr5tc2.Append(new Paragraph(new Run(new Text(oProject.PrimePartner))));
                    tr5.Append(tr5tc2);

                    table.Append(tr5);
                    //----------------------------------------


                    //-----------Funding Account Row
                    TableRow tr6 = new TableRow();
                    TableCell tr6tc1 = new TableCell();
                    tr6tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                    tr6tc1.Append(new Paragraph(new Run(new Text("Project Description:"))));
                    tr6.Append(tr6tc1);

                    TableCell tr6tc2 = new TableCell();
                    tr6tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                    tr6tc2.Append(new Paragraph(new Run(new Text(oProject.ProjectDescription))));
                    tr6.Append(tr6tc2);

                    table.Append(tr6);
                    //----------------------------------------
                    TableRow tr7 = new TableRow();
                    TableCell tr7tc1 = new TableCell();
                    tr7tc1.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));
                    tr7tc1.Append(new Paragraph(new Run(new Text("Country:"))));
                    tr7.Append(tr7tc1);

                    TableCell tr7tc2 = new TableCell();
                    tr7tc2.Append(new TableCellProperties(new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "7000" }));
                    tr7tc2.Append(new Paragraph(new Run(new Text(oProject.Countryname))));
                    tr7.Append(tr7tc2);

                    table.Append(tr7);
                    body.Append(table);
                }
            }

            /////////////
        }
    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        if (!(ddlSectors.SelectedItem == null))
        {
            if (!(ddlSectors.SelectedItem.Text.ToString() == ""))
            {
                sSector = ddlSectors.SelectedItem.Text;
            }
        }

        if (!(dllFprograme.SelectedItem == null))
        {
            if (!(dllFprograme.SelectedItem.Text.ToString() == ""))
            {
                sProgrameArea = dllFprograme.SelectedItem.Text;
            }
        }

        ExportSectorReport(sSector, sProgrameArea);
    }
    private void ExportSectorReport(string sSector, string sProgrameArea)
    {
        SectorsRep rep1 = new SectorsRep();      
        DataSet dsDefaultSearch = rep1.GetSectorsBySearch(sSector, sProgrameArea,"get_ProjectListBySectors");      

        if (dsDefaultSearch != null && dsDefaultSearch.Tables.Count > 0 && dsDefaultSearch.Tables[0].Rows.Count != 0)
          {
            ExportToExcel(dsDefaultSearch, 0, Response, "Report By Sector");
          }
        else
        {
            ExcelRepMessage.Text = "No Records Found.";
        }      
        
    }  

    private void ExportToExcel(DataSet ds, int TableIndex, HttpResponse Response, string FileName)
    {
        int i, j;
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        // Response.ContentType = "application/vnd.ms-excel";
        string timestamp = Convert.ToString(DateTime.Now);
        timestamp = timestamp.Replace(" ", "_");
        timestamp = timestamp.Replace("/", "_");
        //  timestamp = timestamp.Replace(":", ":");
        FileName = FileName.Replace(" ", "");

        string ExtractName = "Projects" + "(" + timestamp + ")" + FileName + " .xlsx";
        //  Response.AppendHeader("content-disposition", "attachment; filename=" + ExtractName + ".xls");
        string sDest = @"E:\xx\SampleExcel\Docs\padp.xlsx";
        string sTempFileName = Server.MapPath(@"~\tmp\dataeX_") + Guid.NewGuid().ToString() + ".xlsx";

        ExportDataSet(ds, sTempFileName);
        displayExport(sTempFileName, ExtractName);

    }

    private void PrepareGridViewForExport(System.Web.UI.Control control)
    {
        string content = "";
        for (int i = control.Controls.Count - 1; i >= 0; i--)
        {
            PrepareGridViewForExport(control.Controls[i]);
        }
        if (!(control is TableCell))
        {
            if (control.GetType().GetProperty("SelectedItem") != null)
            {
                LiteralControl literal = new LiteralControl();
                control.Parent.Controls.Add(literal);
                try
                {
                    literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                }
                catch
                {
                }
                control.Parent.Controls.Remove(control);
            }
            else
                if (control.GetType().GetProperty("Text") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                   // if (control.GetType() == typeof(CheckBox) || control.GetType() == typeof(RadioButton))
                    if (control.GetType() == typeof(CheckBox) )
                    {
                        literal.Text = (bool)control.GetType().GetProperty("Checked").GetValue(control, null) ? "Yes" : "";
                    }
                    else
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);

                    control.Parent.Controls.Remove(control);
                }
        }
        return;
    }
    public static void ExportDataSet(DataSet ds, string destination)
    {
        using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
        {
            var workbookPart = workbook.AddWorkbookPart();

            workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

            foreach (System.Data.DataTable table in ds.Tables)
            {

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                uint sheetId = 1;
                if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                {
                    sheetId =
                        sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }

                DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }


                sheetData.AppendChild(headerRow);

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (String col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        dynamic dc = dsrow[col];
                        if (dc.GetType().ToString() == "System.Boolean")
                        {
                            if (dsrow[col].ToString() == "True")
                            {
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Yes");
                            }

                        }
                        else
                        {
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString());
                        }
                        //if (Type.GetType(dc.GetType().ToString()))
                        //
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

            }
        }
    }
    private void displayExport(string sTempFileName, string sFilename)
    {
        try
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.spreadsheet";
            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);
            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);

            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);

            }
        }
    }
    protected void ddlSectors_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void dllFprograme_SelectedIndexChanged(object sender, EventArgs e)
    {
        ExcelRepMessage.Text = "";
    }
    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        ddlSectors.SelectedIndex = -1;
        dllFprograme.SelectedIndex = -1;
        ExcelRepMessage.Text = "";

    }
}
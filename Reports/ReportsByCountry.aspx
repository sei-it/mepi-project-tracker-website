﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReportsByCountry.aspx.cs" Inherits="Reports_ReportsByCountry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>
        Projects By Country, Program Area, Agency
    </h2>
    <p>
        Filter by Country
        <asp:DropDownList ID="ddlCountry" runat="server"  AutoPostBack="true"  OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
    </p>

      <p>
        Filter by Agency
        <asp:DropDownList ID="cboAgency" runat="server"  AutoPostBack="true"  OnSelectedIndexChanged="cboAgency_SelectedIndexChanged"></asp:DropDownList>
    </p>

    <p>
        Filter by Program Area
        <asp:DropDownList ID="cboProgramAreas" runat="server"  AutoPostBack="true"  OnSelectedIndexChanged="cboProgramAreas_SelectedIndexChanged"></asp:DropDownList>
    </p>
  
    <p>
        <asp:Button ID="btnPrint" runat="server" Text="Report In Word" OnClick="btnPrint_Click" />
        <asp:Button ID="btnExcel" runat="server" Text="Report In Excel" OnClick="btnExcel_Click" />
        <asp:Button ID="btnClearAll" runat="server" Text="Clear all" OnClick="btnClearAll_Click" />
    </p>
    <p><asp:Label runat="server" ID="ExcelRepMessage" ForeColor="Red"></asp:Label></p>
</asp:Content>


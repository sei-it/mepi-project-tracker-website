﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReportCustomGen.aspx.cs" Inherits="ReportCustomGen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
 <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/themes/south-street/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
          <script>
              $(function () {

                  $("#<%=txtStartDateFrom.ClientID%>").datepicker();
                  $("#<%=txtStartDateTo.ClientID%>").datepicker();
                  $("#<%=txtEndDateFrom.ClientID%>").datepicker();
                  $("#<%=txtEndDateTo.ClientID%>").datepicker();

              });
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<h2>Report Generator</h2>
     <h3>Choose Fields</h3>
        <div class="formLayoutBoxes" style="display:none">
            <p>
                <asp:CheckBoxList ID="chkOutputfields" runat="server" RepeatColumns="3">
                    <asp:ListItem Value="AgencyAbbr">Agency</asp:ListItem>
                    <asp:ListItem Value="PrimePartner">Prime Parner</asp:ListItem>
                    <asp:ListItem Value="ProjectName">Project Name</asp:ListItem>
                    <asp:ListItem Value="ProjectDescription">Project Description</asp:ListItem>
                    <asp:ListItem Value="FundingAccount">Funding Account</asp:ListItem>
                    <asp:ListItem Value="TotalFunding">Total  Funding</asp:ListItem>
                </asp:CheckBoxList>
            </p>
 

        </div>
        <div class="formLayoutBoxes">
            <p>
                <asp:CheckBox ID="chkFieldAgency" runat="server" />Agency
            </p>
            <p>
                <asp:CheckBox ID="chkFieldTotalFunding" runat="server" />Total Funding
            </p>
            <p>
                <asp:CheckBox ID="chkFieldFundingAcc" runat="server" />Funding Account
            </p>
            <p>
                <asp:CheckBox ID="chkFieldPrjTimeline" runat="server" />Project Timeline
            </p>
            <p>
                <asp:CheckBox ID="chkFieldPrimePartner" runat="server" />Prime Partner
            </p>
            <p>
                <asp:CheckBox ID="chkFieldProjectDescription" runat="server" />Project Description
            </p>
        </div>

     <h3>Choose Filters</h3>
    <div class="formLayoutBoxes">
        <p>
            <asp:CheckBox ID="chkCountry" runat="server" AutoPostBack="true" OnCheckedChanged="chkCountry_CheckedChanged" />Country: <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
        </p>

         <p>
            <asp:CheckBox ID="chkAgency" runat="server" AutoPostBack="true" OnCheckedChanged="chkAgency_CheckedChanged"   />Agency: <asp:DropDownList ID="cboAgency" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboAgency_SelectedIndexChanged"></asp:DropDownList>
        </p>
        <p>
            <asp:CheckBox ID="chkProgramArea" runat="server" AutoPostBack="true" OnCheckedChanged="chkProgramArea_CheckedChanged" />Program Area: <asp:DropDownList ID="cboProgramAreas" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboProgramAreas_SelectedIndexChanged"></asp:DropDownList>
        </p>
       
        <p>
            <asp:CheckBox ID="chkSector" runat="server" AutoPostBack="true" OnCheckedChanged="chkSector_CheckedChanged" />Sector: <asp:DropDownList ID="cboSector" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboSector_SelectedIndexChanged"></asp:DropDownList>
        </p>
        <p>
            <asp:CheckBox ID="chkPrimeParner" runat="server" AutoPostBack="true"  OnCheckedChanged="chkPrimeParner_CheckedChanged"/>Prime Partner: <asp:TextBox ID="txtPrimePartner" runat="server" AutoPostBack="true" OnTextChanged="txtPrimePartner_TextChanged"></asp:TextBox>
        </p>
        <p style="display:none">
            <asp:CheckBox ID="chkSubImplementingPartner" AutoPostBack="true" runat="server"  OnCheckedChanged="chkSubImplementingPartner_CheckedChanged"/>Sub  Partner: <asp:TextBox ID="txtSubImplementingPartner" AutoPostBack="true" runat="server" OnTextChanged="txtSubImplementingPartner_TextChanged"></asp:TextBox>
        </p>

        <p>
            <asp:CheckBox ID="chkStartDate" runat="server" AutoPostBack="true" OnCheckedChanged="chkStartDate_CheckedChanged" />Start Date: <asp:TextBox ID="txtStartDateFrom" runat="server" AutoPostBack="true" OnTextChanged="txtStartDateFrom_TextChanged"></asp:TextBox><asp:TextBox ID="txtStartDateTo" AutoPostBack="true" runat="server" OnTextChanged="txtStartDateTo_TextChanged"></asp:TextBox>
        </p>
        <p>
            <asp:CheckBox ID="chkEndDate" runat="server" AutoPostBack="true" OnCheckedChanged="chkEndDate_CheckedChanged" />End Date: <asp:TextBox ID="txtEndDateFrom" runat="server" AutoPostBack="true" OnTextChanged="txtEndDateFrom_TextChanged"></asp:TextBox><asp:TextBox ID="txtEndDateTo" runat="server" AutoPostBack="true" OnTextChanged="txtEndDateTo_TextChanged"></asp:TextBox>
        </p>
    </div>
        <p>
                <asp:GridView ID="grdVwList" runat="server"  CssClass="gridTbl" 
            AutoGenerateColumns="true"   
            AllowSorting="false"     >
        </asp:GridView>
    </p>
    <p>
     <asp:Button ID="btnSearch" runat="server" Text="Report In Word" OnClick="btnSearch_Click" />
     <asp:Button ID="btnExcel" runat="server" Text="Report In Excel" OnClick="btnExcel_Click" />
     <asp:Button ID="btnClearAll" runat="server" Text="Clear all" OnClick="btnClearAll_Click" />
    </p>
    <p><asp:Label runat="server" ID="ExcelRepMessage" ForeColor="Red"></asp:Label></p>
</asp:Content>


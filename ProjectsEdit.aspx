﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ProjectsEdit.aspx.cs" Inherits="ProjectsEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">


<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/themes/south-street/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
          <script>
              $(function () {

                  $("#<%=txtStartDate.ClientID%>").datepicker();
                  $("#<%=txtEndDate.ClientID%>").datepicker();

              });
  </script>
    <script type="text/javascript">

        function ValidateModuleList(source, args) {
            var chkListModules = document.getElementById('<%= chkListCountries.ClientID %>');
             var chkListinputs = chkListModules.getElementsByTagName("input");
             for (var i = 0; i < chkListinputs.length; i++) {
                 if (chkListinputs[i].checked) {
                     args.IsValid = true;
                     return;
                 }
             }
             args.IsValid = false;
         }

         function ValidateSectorList(source, args) {
             var chkListModules = document.getElementById('<%= chkListSectors.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>Project Details</h2>
    <div class="formLayout">


         <div>

            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="save" runat="server" ForeColor="Red"
                HeaderText="Please correct the entries below and submit" /><br />
        </div>
        <p>
            <label for="cboAgency">
                Agency/Office<span style="color: red">*</span>
            </label>

            <asp:DropDownList ID="cboAgency" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="reqAgency" runat="server" ForeColor="Red" ValidationGroup="save" defaultValue=" "
                ControlToValidate="cboAgency" ErrorMessage="Agency/Office is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtProjectName">Project Name<span style="color: red">*</span></label>
            <asp:TextBox ID="txtProjectName" runat="server" Width="650px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqProjectName" runat="server" ForeColor="Red" ValidationGroup="save"
                ControlToValidate="txtProjectName" ErrorMessage="Project Name is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtPrimePartner">
                Prime Implementing Partner<span style="color: red">*</span>
            </label>
            <asp:TextBox ID="txtPrimePartner" runat="server" Width="650px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqPrimePartner" runat="server" ForeColor="Red" ValidationGroup="save"
                ControlToValidate="txtPrimePartner" ErrorMessage="Prime Implementing Partner is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtSubImplementingPartner">
                Sub-Implementing Partner
            </label>
            <asp:TextBox ID="txtSubImplementingPartner" runat="server" Width="650px"></asp:TextBox>
        </p>
        <p>
            <label for="txtProjectDescription">Project Description<span style="color: red">*</span></label>
            <asp:TextBox ID="txtProjectDescription" runat="server" Width="650px" TextMode="MultiLine"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqProjectDescription" runat="server" ForeColor="Red" ValidationGroup="save"
                ControlToValidate="txtProjectDescription" ErrorMessage="Project Description is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtFundingAccount">Funding Account<span style="color: red">*</span></label>
         <%--   <asp:TextBox ID="txtFundingAccount" runat="server"></asp:TextBox>--%>
             <asp:DropDownList ID="cboFundingAccount" AutoPostBack="true" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="reqFundingAccount" runat="server" ForeColor="Red" ValidationGroup="save" defaultValue=" "
                ControlToValidate="cboFundingAccount" ErrorMessage="Funding Account is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtTotalFunding">Total Funding<span style="color: red">*</span></label>
            <asp:TextBox ID="txtTotalFunding" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqTotalFunding" runat="server" ForeColor="Red" ValidationGroup="save"
                ControlToValidate="txtTotalFunding" ErrorMessage="Total Funding is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtExpenditure">Expenditure</label>
            <asp:TextBox ID="txtExpenditure" runat="server"></asp:TextBox>

        </p>
        <p>
            <label for="txtUnliquidatedObligation">Unliquidated Obligation</label>
            <asp:TextBox ID="txtUnliquidatedObligation" runat="server"></asp:TextBox>
        </p>
        <p>
            <label for="txtMostRecentObligation">Most Recent Obligation</label>
            <asp:TextBox ID="txtMostRecentObligation" runat="server"></asp:TextBox>
        </p>
        <p>
            <label for="txtFYMostRecentObligation">FY of Most Recent Obligation</label>
            <asp:TextBox ID="txtFYMostRecentObligation" runat="server"></asp:TextBox>
        </p>
        <p>
            <label for="txtStartDate">Start Date<span style="color: red">*</span></label>
            <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqStartDate" runat="server" ForeColor="Red" ValidationGroup="save"
                ControlToValidate="txtStartDate" ErrorMessage="Start Date is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtEndDate">End Date<span style="color: red">*</span></label>
            <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqEndDate" runat="server" ForeColor="Red" ValidationGroup="save"
                ControlToValidate="txtEndDate" ErrorMessage="End Date is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="cboStatus">Project Status<span style="color: red">*</span> </label>
            <asp:DropDownList ID="cboStatus" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="reqStatus" runat="server" ForeColor="Red" ValidationGroup="save"
                ControlToValidate="cboStatus" ErrorMessage="Project Status is Required!">Required!</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="cboProgramAreas">F Program Area<span style="color: red">*</span></label>
            <asp:DropDownList ID="cboProgramAreas" runat="server"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="reqProgramAreas" runat="server" ForeColor="Red" ValidationGroup="save" defaultValue=" "
                ControlToValidate="cboProgramAreas" ErrorMessage="F Program Area is Required!">Required!</asp:RequiredFieldValidator>
        </p>

        <p>
            <label for="txtUSGNEAssistanceGoal">USG/NEA Assistance Goal<span style="color: red">*</span></label>
            <asp:TextBox ID="txtUSGNEAssistanceGoal" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqUSGNEAssistanceGoal" runat="server" ForeColor="Red" ValidationGroup="save"  
                ControlToValidate="txtUSGNEAssistanceGoal" ErrorMessage="USG/NEA Assistance Goal is Required!">Required!</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="rxpvalUSGNEA" runat="server" ForeColor="Red" ValidationGroup="save" 
               ControlToValidate="txtUSGNEAssistanceGoal" ValidationExpression="^\d+(\.\d{1,2})?$" ErrorMessage="Numeric value is required for USG/NEA Assistance Goal">Numeric data up to 2 decimal places is required!</asp:RegularExpressionValidator>
        </p>
       <%-- <p>
            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="save" OnClick="btnSave_Click" />&nbsp;<asp:Button
                ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" Visible="true" />&nbsp;&nbsp;&nbsp;
          
        </p>--%>


    </div>
 <div class="country-contain">
    <div class="country-area">   
    <h3>Country<span style="color: red">*</span>
    </h3>
       <div class="formLayout">
        <asp:CustomValidator runat="server" ID="cvmodulelist" ValidationGroup="save" ForeColor="Red"  ClientValidationFunction="ValidateModuleList" ErrorMessage="Please select atleast one Country" >Required!</asp:CustomValidator>
        <asp:CheckBoxList ID="chkListCountries" runat="server" RepeatColumns="5" RepeatDirection="Horizontal"></asp:CheckBoxList>
        </div>
     </div>   
 </div> 
    <div>
        <h3>Sector<span style="color: red">*</span>
        </h3>
        <div class="formLayout">
            <asp:CustomValidator runat="server" ID="cvSectors" ValidationGroup="save" ForeColor="Red"  ClientValidationFunction="ValidateSectorList" ErrorMessage="Please select atleast one Sector" >Required!</asp:CustomValidator>
            <asp:CheckBoxList ID="chkListSectors" runat="server" RepeatColumns="2"></asp:CheckBoxList>  
             <p>
            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="save" OnClick="btnSave_Click" />&nbsp;<asp:Button
                ID="btnClose" runat="server" Text="Cancel" OnClick="btnClose_Click" Visible="true" />&nbsp;&nbsp;&nbsp;
          
        </p>        
        </div>
        <div>

            <br />
            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="save" runat="server" ForeColor="Red"
                HeaderText="Please correct the entries below and submit" />
        </div>
    </div>
</asp:Content>


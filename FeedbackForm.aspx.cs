﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;




public partial class FeedbackForm : System.Web.UI.Page
{
    int lngPkID;
    string message;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;


        if (ViewState["IsLoaded1"] == null)
        {
            lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
        }

        if (Page.Request.QueryString["Message"] != null)
        {
           message  = Page.Request.QueryString["Message"];
           lblMessage.Text = message;
        }

        loadrecords();
        if (ViewState["IsLoaded1"] == null)
        {
            loadDropdown();
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Page.MaintainScrollPositionOnPostBack = true;

    }

    protected void loadrecords()
    {
    }

    protected void displayRecords()
    {
        object objVal = null;
        DataClasses2DataContext db = new DataClasses2DataContext();
        var oPages = from pg in db.AppFeedbacks
                     where pg.FeedbackID == lngPkID
                     select pg;
        foreach (var oResSubmission in oPages)
        {
            objVal = oResSubmission.FeedbackTypeIDFK;

            if (objVal != null)
            {
                cboFeedbackType.SelectedIndex = cboFeedbackType.Items.IndexOf(cboFeedbackType.Items.FindByValue(oResSubmission.FeedbackTypeIDFK.ToString()));                
            }
            objVal = oResSubmission.Subject;
            if (objVal != null)
            {
                txtSubject.Text = objVal.ToString();
            }
  
            objVal = oResSubmission.Description;
            if (objVal != null)
            {
                txtDescription.Text = objVal.ToString();
            }
            objVal = oResSubmission.URL;
            if (objVal != null)
            {
                txtURL.Text = objVal.ToString();
            }
            objVal = oResSubmission.Phone;
            if (objVal != null)
            {
                txtPhone.Text = objVal.ToString();
            }
            objVal = oResSubmission.Email;
            if (objVal != null)
            {
                txtEmail.Text = objVal.ToString();
            }
        }

        db.Dispose();
          
    }


    protected void loadDropdown()
    {

        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.AppFeedbackTypes 
                      select e;
            cboFeedbackType.DataSource = qry;
            cboFeedbackType.DataTextField = "FeedbackType";
            cboFeedbackType.DataValueField = "FeedbackTypeID";
            cboFeedbackType.DataBind();
            cboFeedbackType.Items.Insert(0, "Select");
 
        }
    }
    public void updatePoster()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        DataClasses2DataContext db = new DataClasses2DataContext();
        AppFeedback oResSubmission = (from c in db.AppFeedbacks where c.FeedbackID == lngPkID select c).FirstOrDefault();
        //ResourceSubmission oResSubmission = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
        if ((oResSubmission == null))
        {
            oResSubmission = new AppFeedback();
            blNew = true;
        }


        if (!(cboFeedbackType.SelectedItem == null))
        {
            if (!(cboFeedbackType.SelectedItem.Value.ToString() == "Select"))
            {
                oResSubmission.FeedbackTypeIDFK = Convert.ToInt32(cboFeedbackType.SelectedItem.Value.ToString());
            }
            else
            {
                oResSubmission.FeedbackTypeIDFK = null;
            }
        }

        oResSubmission.Subject = txtSubject.Text;
 
        oResSubmission.Description = txtDescription.Text;
        oResSubmission.URL = txtURL.Text;
        oResSubmission.Phone = txtPhone.Text;
        oResSubmission.Email = txtEmail.Text;

        oResSubmission.UpdatedBy = HttpContext.Current.User.Identity.Name;

        oResSubmission.UpdatedDt = DateTime.Now;
        //////////////////////
        if (blNew == true)
        {
            oResSubmission.CreatedBy = HttpContext.Current.User.Identity.Name;

            oResSubmission.CreatedOn = DateTime.Now; 
            db.AppFeedbacks.InsertOnSubmit(oResSubmission);


        }

        db.SubmitChanges();
        lngPkID = oResSubmission.FeedbackID;
        db.Dispose();
    

    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
        if (((this.ViewState["Message"] != null)))
        {
            message =Convert.ToString(this.ViewState["Message"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["Message"] = lngPkID;        
        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        updatePoster();
        displayRecords();
        lblMessage.Text = "Your feedback has been submitted successfully";
        sEmail();
       
        //Response.Redirect("Projectslist.aspx?ID=" + lngPkID);
        Response.Redirect("FeedbackForm.aspx?Message="+ lblMessage.Text);    
    
    }
    private void sEmail()
    {
        try
        {
           
            string strFrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
            string strTo = "Vkothale@seiservices.com";
            MailMessage objMailMsg = new MailMessage(strFrom, strTo);
            objMailMsg.CC.Add(new MailAddress("FLacerda@seiservices.com"));

            objMailMsg.BodyEncoding = Encoding.UTF8;
            objMailMsg.Subject = "A New Feedback Form MEPI Project Tracker Has Been Submitted.";

            //-------Email body-----------------------
            
            objMailMsg.Body = "Dear manager, A grantee has submitted a new feedback information.";
            //-------------------
               
            
           

            //-------------------
            objMailMsg.Priority = MailPriority.Low;
            objMailMsg.IsBodyHtml = true;

            //prepare to send mail via SMTP transport
            SmtpClient objSMTPClient = new SmtpClient();

            objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
            NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
            objSMTPClient.Send(objMailMsg);
           
        }
        catch (System.Exception ex)
        {
            //ILog Log = LogManager.GetLogger("EventLog");
            //Log.Fatal("Send report email.", ex);
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

       // Response.Redirect("ProjectsList.aspx");
        Response.Redirect("Default.aspx");
    }



}
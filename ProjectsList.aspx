﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ProjectsList.aspx.cs" Inherits="ProjectsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">   
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script> 
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#fakeLink").click(function () {
                $("#adiv").show(2000);
            });
        });
</script>

  

    <script type="text/javascript">
        function hideshow(which) {
            if (!document.getElementById)
                return
            if (which.style.display == "none")
                which.style.display = "block"
            else
                which.style.display = "none"
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>Project List</h2>
    <div style="float: right">
        <%-- <asp:Label ID="lblSearch" runat="server" Text="Search" Font-Bold="true" Height="25px"> </asp:Label>--%>
        <asp:TextBox ID="txtSearch" runat="server" MaxLength="100" Height="25px"></asp:TextBox>
        <asp:Button ID="btnSearch" class="go-button" runat="server" Text="Go" OnClick="btnSearch_Click" Height="32px" Width="41px" />
    </div>
    <asp:Button ID="btnNew" runat="server" Text="New Project Record" OnClick="btnNew_Click" />
     <p></p>
    <%--<a href="javascript:hideshow(document.getElementById('adiv'))" style="font-size: medium">Click here for custom search</a>--%>

    <asp:LinkButton ID="hideShow" runat="server"  OnClick="hideShow_Click" >Click here for custom search</asp:LinkButton>
   <%--  <span class="fakeLink" id="fakeLink">Login</span>--%>
    <p></p>
    <div id="adiv" style="display: none" class="formLayoutBoxes" runat="server">

        <p>
            <asp:RadioButtonList ID="ActiveProjectList" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" >
                <asp:ListItem Text="Active Projects" Value="1"></asp:ListItem>
                <asp:ListItem Text="Inactive Projects" Value="2"></asp:ListItem>
                <asp:ListItem Text="All Projects" Value="3"></asp:ListItem>
            </asp:RadioButtonList>
        </p>


        <p>
            Country:
                <asp:DropDownList ID="ddlCountry1" runat="server"></asp:DropDownList>

            Agency:
                <asp:DropDownList ID="cboAgency" runat="server"></asp:DropDownList>
        </p>
        <p>
            Program Area:
                <asp:DropDownList ID="cboProgramAreas" runat="server"></asp:DropDownList>
        </p>
        <p>
            Prime Partner:
                <asp:TextBox ID="txtPrimePartner" runat="server" Width="420px"></asp:TextBox>
        </p>
        <p>
            Project Name:
                <asp:TextBox ID="txtProjectName" runat="server" Width="420px"></asp:TextBox>
        </p>

        <p>
            <asp:Button ID="btnCustSearch"  runat="server" Text="Search" OnClick="btnCustSearch_Click" />
             <asp:Button ID="btnClearAll"   runat="server" Text="Clear All" OnClick="btnClearAll_Click"/>
        </p>
    </div>
   
   <br />
    <asp:GridView ID="grdVwList" runat="server" CssClass="gridTblnew"
        AutoGenerateColumns="False" OnSorting="grdVwList_Sorting" AllowSorting="true" 
        OnRowCommand="grdVwList_RowCommand" OnRowDataBound="grdVwList_RowDataBound">
         
        <Columns>

            <asp:BoundField DataField="AgencyAbbr" HeaderText="Agency" SortExpression="AgencyAbbr" DataFormatString="{0:g}">
                <HeaderStyle Width="75px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="ProjectName" HeaderText="Project Name" SortExpression="ProjectName" DataFormatString="{0:g}">
                <HeaderStyle Width="150px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="PrimePartner" HeaderText="Prime Partner" SortExpression="PrimePartner" DataFormatString="{0:g}">
                <HeaderStyle Width="150px"></HeaderStyle>
            </asp:BoundField>

            <asp:TemplateField>
                <HeaderTemplate>
                    Country(ies)                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Repeater ID="repCountry" runat="server">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>

                            <li><%# Eval("Cname") %></li>
                        </ItemTemplate>

                        <FooterTemplate></ul></FooterTemplate>

                    </asp:Repeater>
                </ItemTemplate>
                <HeaderStyle Width="150px" />
            </asp:TemplateField>

            <asp:BoundField DataField="TotalFunding" HeaderText="Total Funding"  DataFormatString="{0:c}" ItemStyle-HorizontalAlign="right">
                <HeaderStyle Width="75px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FundingAccount" HeaderText="Funding Account" SortExpression="FundingAccount"  DataFormatString="{0:g}">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="ProgramArea" HeaderText="Program Area" SortExpression="ProgramArea"  DataFormatString="{0:g}">
                <HeaderStyle Width="125px"></HeaderStyle>
            </asp:BoundField>

            <asp:TemplateField>
                <HeaderTemplate>
                    Edit Project Details                    
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Begin" Visible="true" CssClass="otherstyle" CommandArgument='<%# Eval("ProjectID") %>'>Project Details</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>


        </Columns>
    </asp:GridView>

</asp:Content>


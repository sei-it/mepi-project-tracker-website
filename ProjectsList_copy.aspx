﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ProjectsList.aspx.cs" Inherits="ProjectsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
            <h2>Project List</h2>
    <div style="float:right">
     <%-- <asp:Label ID="lblSearch" runat="server" Text="Search" Font-Bold="true" Height="25px"> </asp:Label>--%>
      <asp:TextBox ID="txtSearch" runat="server" MaxLength="100" Height="25px" ></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" Text="Go"  OnClick="btnSearch_Click" Height="32px" Width="41px" />
    </div>      
        <asp:Button ID="btnNew" runat="server"   Text="New Project Record" OnClick="btnNew_Click" />  
          <div style="padding-top:10px">        
          <asp:RadioButtonList ID="ActiveProjectList" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="ActiveProjectList_SelectedIndexChanged">
                    <asp:ListItem Text="Active Projects" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Inactive Projects" Value="2"></asp:ListItem>
                    <asp:ListItem Text="All Projects" Value="3"></asp:ListItem>
          </asp:RadioButtonList>

              </div> 

   
    
         <asp:GridView ID="grdVwList" runat="server"  CssClass="gridTbl" 
            AutoGenerateColumns="False"   
            AllowSorting="false" OnRowCommand="grdVwList_RowCommand" OnRowDataBound="grdVwList_RowDataBound"   >
            <Columns>
 
		            <asp:BoundField DataField="AgencyAbbr" HeaderText="Agency" DataFormatString="{0:g}" >
		            <HeaderStyle Width="75px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="ProjectName" HeaderText="Project Name" DataFormatString="{0:g}" >
                        		            <HeaderStyle Width="150px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="PrimePartner" HeaderText="Prime Partner" DataFormatString="{0:g}" >
                        		            <HeaderStyle Width="150px"></HeaderStyle>
		            </asp:BoundField>

                     <asp:TemplateField>
                       <HeaderTemplate>   
                           Country(ies)                    
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:Repeater ID="repCountry" runat="server">
                                    <HeaderTemplate>
                                        <ul>
                                    </HeaderTemplate>
                                    <ItemTemplate> 

                                        <li><%# Eval("Cname") %></li>
                                    </ItemTemplate>
 
                                    <FooterTemplate></ul></FooterTemplate>
                                    
                                </asp:Repeater>                         
                            </ItemTemplate>
                           <HeaderStyle width="150px" />
                        </asp:TemplateField>   

		            <asp:BoundField DataField="TotalFunding" HeaderText="Total Funding"  DataFormatString="{0:c}" ItemStyle-HorizontalAlign="right"  >
                        		            <HeaderStyle Width="75px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="FundingAccount" HeaderText="Funding Account" DataFormatString="{0:g}" >
                        		            <HeaderStyle Width="100px"></HeaderStyle>
		            </asp:BoundField>
		            <asp:BoundField DataField="ProgramArea" HeaderText="Program Area" DataFormatString="{0:g}" >
                        		            <HeaderStyle Width="125px"></HeaderStyle>
		            </asp:BoundField>

                     <asp:TemplateField>
                       <HeaderTemplate>  
                         Edit Project Details                    
                        </HeaderTemplate>                        
                            <ItemTemplate>               
                                <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="Begin" visible="true"  CssClass="otherstyle" CommandArgument='<%# Eval("ProjectID") %>'>Project Details</asp:LinkButton>
                                                          
                            </ItemTemplate>
                           <HeaderStyle width="100px" />
                        </asp:TemplateField>  
 
 
            </Columns>
        </asp:GridView>
        
</asp:Content>


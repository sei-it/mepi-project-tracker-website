﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ProjectsEdit : System.Web.UI.Page
{
    int lngPkID;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;


        if (ViewState["IsLoaded1"] == null)
        {
            lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
        }

        loadrecords();
        if (ViewState["IsLoaded1"] == null)
        {
            loadDropdown();
            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Page.MaintainScrollPositionOnPostBack = true;

    }

    protected void loadrecords()
    {
    }

    protected void displayRecords()
    {
        object objVal = null;
        DataClasses2DataContext db = new DataClasses2DataContext();
        var oPages = from pg in db.Projects                    
                     where pg.ProjectID == lngPkID
                     select pg;
                     
        foreach (var oResSubmission in oPages)
        {


            objVal = oResSubmission.AgencyID_fk;


            if (objVal != null)
            {
                cboAgency.SelectedIndex = cboAgency.Items.IndexOf(cboAgency.Items.FindByValue(oResSubmission.AgencyID_fk.ToString()));
            }

            objVal = oResSubmission.ProjectName;
            if (objVal != null)
            {
                txtProjectName.Text = objVal.ToString();
            }
            objVal = oResSubmission.PrimePartner;
            if (objVal != null)
            {
                txtPrimePartner.Text = objVal.ToString();
            }
            objVal = oResSubmission.SubImplementingPartner;
            if (objVal != null)
            {
                txtSubImplementingPartner.Text = objVal.ToString();
            }
            objVal = oResSubmission.ProjectDescription;
            if (objVal != null)
            {
                txtProjectDescription.Text = objVal.ToString();
            }
           // objVal = oResSubmission.FundingAccount;
            objVal = oResSubmission.FundingAccountID_fk;
            if (objVal != null)
            {
               // txtFundingAccount.Text = objVal.ToString();
                cboFundingAccount.SelectedValue = Convert.ToString(oResSubmission.FundingAccountID_fk);
            }
            objVal = oResSubmission.TotalFunding;
            if (objVal != null)
            {
                txtTotalFunding.Text = objVal.ToString();
            }
            objVal = oResSubmission.Expenditure;
            if (objVal != null)
            {
                txtExpenditure.Text = objVal.ToString();
            }
            objVal = oResSubmission.UnliquidatedObligation;
            if (objVal != null)
            {
                txtUnliquidatedObligation.Text = objVal.ToString();
            }
            objVal = oResSubmission.MostRecentObligation;
            if (objVal != null)
            {
                txtMostRecentObligation.Text = objVal.ToString();
            }
            objVal = oResSubmission.FYMostRecentObligation;
            if (objVal != null)
            {
                txtFYMostRecentObligation.Text = objVal.ToString();
            }
            objVal = oResSubmission.StartDate;
            if (objVal != null)
            {
                txtStartDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
            }
            objVal = oResSubmission.EndDate;
            if (objVal != null)
            {
                txtEndDate.Text = String.Format("{0:MM/dd/yyyy}", objVal);
            }
            //objVal = oResSubmission.FProgramArea;
            //if (objVal != null)
            //{
            //    txtFProgramArea.Text = objVal.ToString();
            //}

            objVal = oResSubmission.ProgramAreaID_fk;


            if (objVal != null)
            {
                cboProgramAreas.SelectedIndex = cboProgramAreas.Items.IndexOf(cboProgramAreas.Items.FindByValue(oResSubmission.ProgramAreaID_fk.ToString()));
            }
            objVal = oResSubmission.USGNEAssistanceGoal;
            if (objVal != null)
            {
                txtUSGNEAssistanceGoal.Text = objVal.ToString();
            }

            if (objVal != null)
            {
                cboStatus.SelectedIndex = cboStatus.Items.IndexOf(cboStatus.Items.FindByValue(oResSubmission.StatusID_FK.ToString()));
            }

        }

        db.Dispose();
        displayCountries();
        displaySectors();
    }


    protected void loadDropdown()
    {
 
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.CountriesBMENAs
                     orderby e.Countryname
                      select  e;
            chkListCountries.DataSource = qry;
            chkListCountries.DataTextField = "Countryname";
            chkListCountries.DataValueField = "Id";
            chkListCountries.DataBind();



            var qry3 = from e in db.Sectors
                       orderby e.Sector1
                       select e;
            chkListSectors.DataSource = qry3;
            chkListSectors.DataTextField = "Sector1";
            chkListSectors.DataValueField = "SectorID";
            chkListSectors.DataBind();

            var qry4 = from e in db.Agencies
                       orderby e.AgencyAbbr
                       select e;
            cboAgency.DataSource = qry4;
            cboAgency.DataTextField = "AgencyAbbr";
            cboAgency.DataValueField = "AgencyID";
            cboAgency.DataBind();
            cboAgency.Items.Insert(0, "");



            var qry5 = from e in db.ProgramAreas                      
                       orderby e.ProgramArea1
                       select e;
            cboProgramAreas.DataSource = qry5;
            cboProgramAreas.DataTextField = "ProgramArea1";
            cboProgramAreas.DataValueField = "ProgramAreaID";
            cboProgramAreas.DataBind();
            cboProgramAreas.Items.Insert(0, "");

            var qry6 = from e in db.Status
                       orderby e.StatusDescription
                       select e;
            cboStatus.DataSource = qry6;
            cboStatus.DataTextField = "StatusDescription";
            cboStatus.DataValueField = "StatusID";
            cboStatus.DataBind();
            cboStatus.Items.Insert(0, "");


            var qry7 = from e in db.FundingAccounts
                       orderby e.FundingAccount1 
                       select e;
            cboFundingAccount.DataSource = qry7;
            cboFundingAccount.DataTextField = "FundingAccount1";
            cboFundingAccount.DataValueField = "FundingAccountID";
            cboFundingAccount.DataBind();
            cboFundingAccount.Items.Insert(0, "");

        }
    }



    public void updatePoster()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        DataClasses2DataContext db = new DataClasses2DataContext();
        Project oResSubmission = (from c in db.Projects where c.ProjectID == lngPkID select c).FirstOrDefault();
        //ResourceSubmission oResSubmission = (from c in db.ResourceSubmissions where c.ResSubID == 0 select c).First();
        if ((oResSubmission == null))
        {
            oResSubmission = new Project();
            blNew = true;
        }


        if (!(cboAgency.SelectedItem == null))
        {
            if (!(cboAgency.SelectedItem.Value.ToString() == ""))
            {
                oResSubmission.AgencyID_fk = Convert.ToInt32(cboAgency.SelectedItem.Value.ToString());
            }
            else
            {
                oResSubmission.AgencyID_fk = null;
            }
        }


        if (!(cboProgramAreas.SelectedItem == null))
        {
            if (!(cboProgramAreas.SelectedItem.Value.ToString() == ""))
            {
                oResSubmission.ProgramAreaID_fk = Convert.ToInt32(cboProgramAreas.SelectedItem.Value.ToString());
            }
            else
            {
                oResSubmission.ProgramAreaID_fk = null;
            }
        }

        if (!(cboStatus.SelectedItem == null))
        {
            if (!(cboStatus.SelectedItem.Value.ToString() == ""))
            {
                oResSubmission.StatusID_FK = Convert.ToInt32(cboStatus.SelectedItem.Value.ToString());
            }
            else
            {
                oResSubmission.StatusID_FK = null;
            }
        }


         oResSubmission.ProjectName = txtProjectName.Text;
        oResSubmission.PrimePartner = txtPrimePartner.Text;
        oResSubmission.SubImplementingPartner = txtSubImplementingPartner.Text;
        oResSubmission.ProjectDescription = txtProjectDescription.Text;
       // oResSubmission.FundingAccount = txtFundingAccount.Text;

        if (!(cboFundingAccount.SelectedItem == null))
        {
            if (!(cboFundingAccount.SelectedItem.Value.ToString() == ""))
            {
                //oResSubmission.FundingAccount = cboFundingAccount.SelectedItem.Text;
                oResSubmission.FundingAccountID_fk = Convert.ToInt32(cboFundingAccount.SelectedItem.Value);
            }
            else
            {
                oResSubmission.FundingAccountID_fk = null;
            }
        }



        int.TryParse(txtTotalFunding.Text, out strTmp);
        oResSubmission.TotalFunding = strTmp;
        int.TryParse(txtExpenditure.Text, out strTmp);
        oResSubmission.Expenditure = strTmp;
        int.TryParse(txtUnliquidatedObligation.Text, out strTmp);
        oResSubmission.UnliquidatedObligation = strTmp;
        int.TryParse(txtMostRecentObligation.Text, out strTmp);
        oResSubmission.MostRecentObligation = strTmp;
        oResSubmission.FYMostRecentObligation = txtFYMostRecentObligation.Text;
          
        DateTime.TryParse(txtStartDate.Text, out dtTmp);
        if (txtStartDate.Text != "")
        {
            oResSubmission.StartDate = dtTmp;
        }
        else
        {
            oResSubmission.StartDate = null;
        }
        DateTime.TryParse(txtEndDate.Text, out dtTmp); 
        
        if (txtEndDate.Text != "")
        {
            oResSubmission.EndDate = dtTmp;
        }
        else
        {
            oResSubmission.EndDate = null;
        }
       // oResSubmission.FProgramArea = txtFProgramArea.Text;

        //int.TryParse(txtUSGNEAssistanceGoal.Text, out strTmp);
        //oResSubmission.USGNEAssistanceGoal = strTmp;

        decimal duSG = Convert.ToDecimal(txtUSGNEAssistanceGoal.Text);
        oResSubmission.USGNEAssistanceGoal = duSG;

        oResSubmission.updatedBy = HttpContext.Current.User.Identity.Name;

        oResSubmission.updatedDt = DateTime.Now;
        //////////////////////
        if (blNew == true)
        {

            oResSubmission.createdBy = HttpContext.Current.User.Identity.Name;
            oResSubmission.createdDt = DateTime.Now;
            db.Projects.InsertOnSubmit(oResSubmission);
            
            
        }

        db.SubmitChanges();
        lngPkID = oResSubmission.ProjectID;
        db.Dispose();
        saveCountries();
        saveSectors();

    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }


    }
    protected override object SaveViewState()
    {

        this.ViewState["lngPKID"] = lngPkID;
        return (base.SaveViewState());
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {


        updatePoster();
        displayRecords();

        Response.Redirect("Projectslist.aspx?ID=" + lngPkID);

    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        string strJS = null;
        strJS = "<script language=\"JavaScript\">top.returnValue=1;window.close();";
        strJS = strJS + "opener.location=opener.location; </script>";

        Response.Redirect("ProjectsList.aspx");
    }


    //-----Countries----------------//
    protected void loadCountries()
    {

        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from p in db.CountriesBMENAs
                      orderby p.Countryname
                       select p;
            chkListCountries.DataSource = qry;
            chkListCountries.DataTextField = "Countryname";
            chkListCountries.DataValueField = "Id";
            chkListCountries.DataBind();
        }
    }
    private void displayCountries()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {

            var oPages = from pg in db.ProjectCountries
                         where pg.ProjectID == lngPkID && pg.ProjectID !=0
                         select pg;
            foreach (var oCase in oPages)
            {
                for (int i = 0; i < chkListCountries.Items.Count; i++)
                {
                    if (Convert.ToInt32(chkListCountries.Items[i].Value) == oCase.CountryID )
                    {
                        chkListCountries.Items[i].Selected = true;
                    }
                }

            }
        }

    }
    protected void saveCountries()
    {
        for (int i = 0; i < chkListCountries.Items.Count; i++)
        {
            if (chkListCountries.Items[i].Selected == true)
            {
                updateCountries(Convert.ToInt32(chkListCountries.Items[i].Value));
            }
            else
            {
                removeCountries(Convert.ToInt32(chkListCountries.Items[i].Value));
            }
        }
    }
    protected void updateCountries(int intCountryID)
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            Boolean blNew = false;
            ProjectCountry oCase = (from c in db.ProjectCountries
                                            where c.CountryID == intCountryID 
                                                && c.ProjectID == lngPkID
                                            select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new ProjectCountry();
                blNew = true;
                oCase.ProjectID =  lngPkID;
                oCase.CountryID = intCountryID ;
            }

            if (blNew == true)
            {
                db.ProjectCountries.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();

        }
    }
    protected void removeCountries(int intCountryID)
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            Boolean blNew = false;
            ProjectCountry  oCase = (from c in db.ProjectCountries
                                     where c.CountryID == intCountryID
                                                && c.ProjectID  == lngPkID
                                            select c).FirstOrDefault();
            if ((oCase != null))
            {
                db.ProjectCountries.DeleteOnSubmit(oCase);
            }
            db.SubmitChanges();

        }
    }

    //------------------------------------------------------------
    //-----Sectors----------------//

    private void displaySectors()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {

            var oPages = from pg in db.ProjectSectors
                         where pg.ProjectID == lngPkID 
                         select pg;
            foreach (var oCase in oPages)
            {
                for (int i = 0; i < chkListSectors.Items.Count; i++)
                {
                    if (Convert.ToInt32(chkListSectors.Items[i].Value) == oCase.SectorsID)
                    {
                        chkListSectors.Items[i].Selected = true;
                    }
                }

            }
        }

    }
    protected void saveSectors()
    {
        for (int i = 0; i < chkListSectors.Items.Count; i++)
        {
            if (chkListSectors.Items[i].Selected == true)
            {
                updateSectors(Convert.ToInt32(chkListSectors.Items[i].Value));
            }
            else
            {
                removeSectors(Convert.ToInt32(chkListSectors.Items[i].Value));
            }
        }
    }
    protected void updateSectors(int intSectorID)
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            Boolean blNew = false;
            ProjectSector oCase = (from c in db.ProjectSectors
                                   where c.SectorsID == intSectorID
                                                && c.ProjectID == lngPkID 
                                            select c).FirstOrDefault();
            if ((oCase == null))
            {
                oCase = new ProjectSector();
                blNew = true;
                oCase.ProjectID = lngPkID;
                oCase.SectorsID = intSectorID ;
            }

            if (blNew == true)
            {
                oCase.createdBy = HttpContext.Current.User.Identity.Name;
                oCase.createdDt = DateTime.Now;
                db.ProjectSectors.InsertOnSubmit(oCase);
            }

            db.SubmitChanges();

        }
    }
    protected void removeSectors(int intSectorID)
    {
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            Boolean blNew = false;
            ProjectSector  oCase = (from c in db.ProjectSectors 
                                            where c.SectorsID == intSectorID 
                                                && c.ProjectID == lngPkID
                                            select c).FirstOrDefault();
            if ((oCase != null))
            {

                oCase.updatedBy = HttpContext.Current.User.Identity.Name;
                oCase.updatedDt = DateTime.Now;
                db.ProjectSectors.DeleteOnSubmit(oCase);
            }
            db.SubmitChanges();

        }
    }
//---------------------------------------------------------------------------

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.IO;
using DocumentFormat.OpenXml;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for Reports
/// </summary>
public class Reports
{
    string connString = ConfigurationManager.ConnectionStrings["MEPI_ProjectsConnectionString"].ToString();
	public Reports()
	{
		
	}



    public DataSet GetCountryBySearch(string sCountry, string sProgramArea, string sAgency, string get_ProjectListByCountryPAreaAgency)
    {
        
         DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand("get_ProjectListByCountryPAreaAgency", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Country", sCountry);
        cmd.Parameters.Add("@ProgramArea", sProgramArea);
        cmd.Parameters.Add("@Agency", sAgency);
       
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

  

    public DataSet GetReports(string sCountry, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.Parameters.Add("@Country", sCountry);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }
}
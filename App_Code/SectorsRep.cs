﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.IO;
using DocumentFormat.OpenXml;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for SectorsRep
/// </summary>
public class SectorsRep
{
    string connString = ConfigurationManager.ConnectionStrings["MEPI_ProjectsConnectionString"].ToString();
	
	public SectorsRep()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet GetSectorsBySearch(string sSector, string sProgrameArea, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Sector", sSector);
        cmd.Parameters.Add("@ProgramArea", sProgrameArea);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

   

    public DataSet GetCustomReportBySearch(string sFilterCountry, string sFilterPrimeParner, string sFilterProgramArea, string sFilterSubImplementingPartner, string sFilterAgency, string sFilterSector, DateTime? sFilterStartFrom, DateTime? sFilterStartTo, DateTime? sFilterEndFrom, DateTime? sFilterEndTo, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Country", sFilterCountry);
        cmd.Parameters.Add("@PrimePartner", sFilterPrimeParner);
        cmd.Parameters.Add("@ProgramArea", sFilterProgramArea);
        cmd.Parameters.Add("@SubImplementingPartner", sFilterSubImplementingPartner);

        cmd.Parameters.Add("@Agency", sFilterAgency);
        cmd.Parameters.Add("@Sector", sFilterSector);
        cmd.Parameters.Add("@StartDate_FROM", Convert.ToString(sFilterStartFrom));
        cmd.Parameters.Add("@StartDate_TO", Convert.ToString(sFilterStartTo));
        cmd.Parameters.Add("@EndDate_FROM", Convert.ToString(sFilterEndFrom));
        cmd.Parameters.Add("@EndDate_TO", Convert.ToString(sFilterEndTo));


        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }

        if (ds.Tables[0].Rows.Count != 0)
        {
        }
        return ds;

    }

    public DataSet GetStatusBySearch(string sStatus, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Status", sStatus);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet ActiveSearchList1(string sAgency, string sCountry, string sProgramAreas, string sProjectName, string sPrimePartner, string p)
    {
        DataSet dsBySearch = new DataSet();
        SqlConnection con= new SqlConnection(connString);
        SqlCommand cmdInviteesBySearch = new SqlCommand(p, con);
        cmdInviteesBySearch.CommandType = CommandType.StoredProcedure;
        cmdInviteesBySearch.Parameters.Add("@Country", sCountry);
        cmdInviteesBySearch.Parameters.Add("@PrimePartner", sPrimePartner);
        cmdInviteesBySearch.Parameters.Add("@ProjectName", sProjectName);
        cmdInviteesBySearch.Parameters.Add("@Agency", sAgency);
        cmdInviteesBySearch.Parameters.Add("@ProgramArea", sProgramAreas);     

        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmdInviteesBySearch);
            da.Fill(dsBySearch);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return dsBySearch;
    }


    public DataSet InactiveSearchList(string sAgency, string sCountry, string sProgramAreas, string sProjectName, string sPrimePartner, string p)
    {
        DataSet dsBySearch = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmdInviteesBySearch = new SqlCommand(p, con);
        cmdInviteesBySearch.CommandType = CommandType.StoredProcedure;
        cmdInviteesBySearch.Parameters.Add("@Country", sCountry);
        cmdInviteesBySearch.Parameters.Add("@PrimePartner", sPrimePartner);
        cmdInviteesBySearch.Parameters.Add("@ProjectName", sProjectName);
        cmdInviteesBySearch.Parameters.Add("@Agency", sAgency);
        cmdInviteesBySearch.Parameters.Add("@ProgramArea", sProgramAreas);

        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmdInviteesBySearch);
            da.Fill(dsBySearch);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return dsBySearch;
    }

    public DataSet ActiveInactiveSearchList(string sAgency, string sCountry, string sProgramAreas, string sProjectName, string sPrimePartner, string p)
    {
        DataSet dsBySearch = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmdInviteesBySearch = new SqlCommand(p, con);
        cmdInviteesBySearch.CommandType = CommandType.StoredProcedure;
        cmdInviteesBySearch.Parameters.Add("@Country", sCountry);
        cmdInviteesBySearch.Parameters.Add("@PrimePartner", sPrimePartner);
        cmdInviteesBySearch.Parameters.Add("@ProjectName", sProjectName);
        cmdInviteesBySearch.Parameters.Add("@Agency", sAgency);
        cmdInviteesBySearch.Parameters.Add("@ProgramArea", sProgramAreas);

        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmdInviteesBySearch);
            da.Fill(dsBySearch);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return dsBySearch;
    }
}
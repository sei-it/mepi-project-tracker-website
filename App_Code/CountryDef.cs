﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.IO;
using DocumentFormat.OpenXml;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;


/// <summary>
/// Summary description for CountryDef
/// </summary>
public class CountryDef
{
    string connString = ConfigurationManager.ConnectionStrings["MEPI_ProjectsConnectionString"].ToString();
	public CountryDef()
	{
		
	}

    public DataSet GetReports(string sCountry, string p)
    {
        DataSet ds = new DataSet();
        //string sql = "select * from Projects";

        SqlConnection con = new SqlConnection(connString);
        // SqlCommand cmd = new SqlCommand(sql, con);

        SqlCommand cmd = new SqlCommand(p, con);
        cmd.Parameters.Add("@Country", sCountry);
        cmd.CommandType = CommandType.StoredProcedure;
        // cmd.CommandType = CommandType.Text;
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Configuration;
using System.IO;


public partial class ProjectsList : System.Web.UI.Page
{
    string connString = ConfigurationManager.ConnectionStrings["MEPI_ProjectsConnectionString"].ToString();
    string sSearchWord = "";
    int lngPkID;
    int allchecked = 1;
    string sCountry;
    string sAgency;
    string sProgramAreas;
    string sPrimePartner;
    string sProjectName;
    int sSearchFlag;

 
    //100-Admin
    //1-Grantee Admin
    //2-Case Workers
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            ActiveProjectList.SelectedIndex = 0;
            if (Page.Request.QueryString["lngPkID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
            }
 
            displayRecords();
            loadDropdown();
        }

        Page.MaintainScrollPositionOnPostBack = true;

        string strJS;
        //strJS = ("window.open(\"UserEdit.aspx?lngPkID=" + ("0" + "\",\'\',\"width=950,height=700,resizable,scrollbars\");"));
        //btnNew.Attributes.Add("onclick", strJS);
       
    }

    protected void loadDropdown()
    {

        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            var qry = from e in db.CountriesBMENAs
                      select e;
            ddlCountry1.DataSource = qry;
            ddlCountry1.DataTextField = "Countryname";
            ddlCountry1.DataValueField = "Id";
            ddlCountry1.DataBind();
            ddlCountry1.Items.Insert(0, "");

            var qry4 = from e in db.Agencies
                       orderby e.AgencyAbbr
                       select e;
            cboAgency.DataSource = qry4;
            cboAgency.DataTextField = "AgencyAbbr";
            cboAgency.DataValueField = "AgencyID";
            cboAgency.DataBind();
            cboAgency.Items.Insert(0, "");

            var qry5 = from e in db.ProgramAreas                       
                       orderby e.ProgramArea1
                       select e;
            cboProgramAreas.DataSource = qry5;
            cboProgramAreas.DataTextField = "ProgramArea1";
            cboProgramAreas.DataValueField = "ProgramAreaID";
            cboProgramAreas.DataBind();
            cboProgramAreas.Items.Insert(0, "");

           

        }
    }

    private void displayRecords()
    {
        object objVal = null;
        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            // var vCases = db.get_ProjectList(); // commented ob feb 03 2015 
            // HttpContext.Current.User.Identity.Name
            // var vCases = db.get_ProjectListAll();
            // var vCases = db.get_ProjectListDashboard();  // default to active list
            var vCases = db.get_ProjectListForCurrentProject();
            grdVwList.DataSource = vCases;
            grdVwList.DataBind(); 

        }

    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Begin")
        {
            int intCaseID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("ProjectsEdit.aspx?lngPkID=" + intCaseID);
        }
 
    }
    protected void btnNew_Click(object sender, EventArgs e)
    {

        Response.Redirect("ProjectsEdit.aspx");
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }
 

    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
 
        return (base.SaveViewState());
    }

    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        

        Repeater  repCountry;
        int intTmpProjectID=0;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "ProjectID") != null)
            {
                intTmpProjectID = (int)DataBinder.Eval(e.Row.DataItem, "ProjectID");
            }
            repCountry = (Repeater)e.Row.FindControl("repCountry");


            using (DataClasses2DataContext db = new DataClasses2DataContext())
            {
                var qry2 = from u in db.ProjectCountries
                    join ut in db.CountriesBMENAs on u.CountryID equals ut.Id
                           where u.ProjectID == intTmpProjectID
                           select new {Cname=ut.Countryname};
            repCountry.DataSource = qry2;
            repCountry.DataBind();
            }

        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        getSearchWord();

        using (DataClasses2DataContext db = new DataClasses2DataContext())
        {
            if (ActiveProjectList.SelectedIndex == 0)
            {
                var vCases = db.get_ActiveProjectListByWord(sSearchWord);
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }            
            else if (ActiveProjectList.SelectedIndex == 1) //inactive list selected
            {
                var vCases = db.get_ProjectListByWord(sSearchWord);
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
            else
            {
                var vCases = db.get_AllProjectListByWord(sSearchWord);
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();

            }
        }
 
    }

    private void getSearchWord()
    {
        if (!(string.IsNullOrEmpty(txtSearch.Text)))
        {
            sSearchWord = txtSearch.Text;
        }
    }
    protected void ActiveProjectList_SelectedIndexChanged(object sender, EventArgs e)
    {
         int i = ActiveProjectList.SelectedIndex;
        allchecked = 0;
        if (i == 0)
        {
            using (DataClasses2DataContext db = new DataClasses2DataContext())
            {
                //var vCases = db.get_ProjectListDashboard();
                var vCases = db.get_ProjectListForCurrentProject();
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
        }
        else if (i == 1)
        {
            using (DataClasses2DataContext db = new DataClasses2DataContext())
            {
                // var vCases = db.get_ProjectListAll();
                var vCases = db.get_InactiveProjectList();
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
        }
        else
        {
            using (DataClasses2DataContext db = new DataClasses2DataContext())
            {
                // var vCases = db.get_ProjectListAll();
                var vCases = db.get_ActiveInactiveProjectList();
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
        }
    }
    protected void grdVwList_Sorting(object sender, GridViewSortEventArgs e)
    {
      
         string sortingDirection = string.Empty;
        if (direction == SortDirection.Ascending)
        {
            direction = SortDirection.Descending;
            sortingDirection = "Desc" ;

        }
        else
        {
            direction = SortDirection.Ascending;
            sortingDirection = "Asc" ;

        }

        // get all the custom search values
        getSearch();
        getSearchWord();

       

        int i = ActiveProjectList.SelectedIndex;
        allchecked = 0;
        if (i == 0)
        {
            DataView sortedView = new DataView();
            //sortedView = BindGridView("get_ProjectListForCurrentProject");
            if (!string.IsNullOrEmpty(sSearchWord))
            {
               // db.get_ActiveProjectListByWord(sSearchWord);
               sortedView = BindGVWordSearch(sSearchWord, "get_ActiveProjectListByWord");
            }
            else
            {
                sortedView = BindGridView(sAgency, sCountry, sProgramAreas, sProjectName, sPrimePartner, "get_ActiveSearchList");
            }           
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            Session["SortedView"] = sortedView;
            grdVwList.DataSource = sortedView;
            grdVwList.DataBind();
        }
        else if (i == 1)
        {
            DataView sortedView1 = new DataView();
            //sortedView1 = BindGridView("get_InactiveProjectList");
           // sortedView1 = BindGridView(sAgency, sCountry, sProgramAreas, sProjectName, sPrimePartner, "get_InactiveSearchList");

            if (!string.IsNullOrEmpty(sSearchWord))
            {
                // db.get_ActiveProjectListByWord(sSearchWord);
                sortedView1 = BindGVWordSearch(sSearchWord, "get_ProjectListByWord");
            }
            else
            {
                sortedView1 = BindGridView(sAgency, sCountry, sProgramAreas, sProjectName, sPrimePartner, "get_InactiveSearchList");
            }   
            sortedView1.Sort = e.SortExpression + " " + sortingDirection;
            Session["SortedView"] = sortedView1;
            grdVwList.DataSource = sortedView1;
            grdVwList.DataBind();
        }
        else if (i == 2)
        {
            DataView sortedView2 = new DataView();
            //sortedView2 = BindGridView("get_ActiveInactiveProjectList");
            // sortedView2 = BindGridView(sAgency, sCountry, sProgramAreas, sProjectName, sPrimePartner, "get_ActiveInactiveSearchList"); 
            if (!string.IsNullOrEmpty(sSearchWord))
            {
                // db.get_ActiveProjectListByWord(sSearchWord);
                sortedView2 = BindGVWordSearch(sSearchWord, "get_AllProjectListByWord");
            }
            else
            {
                sortedView2 = BindGridView(sAgency, sCountry, sProgramAreas, sProjectName, sPrimePartner, "get_InactiveSearchList");
            }  
            sortedView2.Sort = e.SortExpression + " " + sortingDirection;
            Session["SortedView"] = sortedView2;
            grdVwList.DataSource = sortedView2;
            grdVwList.DataBind();
        }
        
    }

    private DataView BindGVWordSearch(string sSearchWord, string p)
    {
        DataSet ds = new DataSet();
        DataView dv1 = new DataView();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@SearchWord", sSearchWord);       
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dv1 = new DataView(ds.Tables[0]);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }

        return dv1;
    }

    private DataView BindGridView(string sAgency, string sCountry, string sProgramAreas, string sProjectName, string sPrimePartner, string p)
    {
        DataSet ds = new DataSet();
        DataView dv = new DataView();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Country", sCountry);
        cmd.Parameters.Add("@PrimePartner", sPrimePartner);
        cmd.Parameters.Add("@ProjectName", sProjectName);
        cmd.Parameters.Add("@Agency", sAgency);
        cmd.Parameters.Add("@ProgramArea", sProgramAreas);     

        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            dv = new DataView(ds.Tables[0]);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }

        return dv;
    }
    public SortDirection direction
    {
        get
        {
            if (ViewState["directionState"] == null)
            {
                ViewState["directionState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["directionState"];
        }
        set
        {
            ViewState["directionState"] = value;
        }
    }    

    //Custom Search
    protected void btnCustSearch_Click(object sender, EventArgs e)
    {
        txtSearch.Text = "";
        SectorsRep rep1 = new SectorsRep();
        DataSet ds = new  DataSet();
        getSearch();
       
        
            if (ActiveProjectList.SelectedIndex == 0)
            {
                    ds = rep1.ActiveSearchList1(sAgency, sCountry, sProgramAreas, sProjectName, sPrimePartner, "get_ActiveSearchList");                
                
                    grdVwList.DataSource = ds;
                    grdVwList.DataBind();
                
            }
            else if (ActiveProjectList.SelectedIndex == 1) //inactive list selected
            {
                var vCases = rep1.InactiveSearchList(sAgency, sCountry, sProgramAreas, sProjectName, sPrimePartner, "get_InactiveSearchList");
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();
            }
            else if (ActiveProjectList.SelectedIndex == 2)
            {
                var vCases = rep1.ActiveInactiveSearchList(sAgency, sCountry, sProgramAreas, sProjectName, sPrimePartner, "get_ActiveInactiveSearchList");
                grdVwList.DataSource = vCases;
                grdVwList.DataBind();

            }

            adiv.Style["display"] = "None";                

    }

    private void getSearch()
    {


        if (cboAgency.SelectedIndex != -1 && cboAgency.SelectedValue != "")
        {
            sAgency = cboAgency.SelectedItem.Text;
           
        }
        else
        {
            sAgency="";
        }


        if (ddlCountry1.SelectedIndex != -1 && ddlCountry1.SelectedValue != "")
        {
            sCountry = ddlCountry1.SelectedItem.Text;
        }
        else
        {
            sCountry ="";
        }


        if (cboProgramAreas.SelectedIndex != -1 && cboProgramAreas.SelectedValue != "")
        {
            sProgramAreas = cboProgramAreas.SelectedItem.Text;
        }
        else
        {
            sProgramAreas = "";
        }


        if (!(string.IsNullOrEmpty(txtProjectName.Text)))
        {
            //sProjectName = txtProjectName.Text.Trim();
            sProjectName = txtProjectName.Text.Trim();
        }
        else
        {
            sProjectName = "";
        }
        if (!(string.IsNullOrEmpty(txtPrimePartner.Text)))
        {
            sPrimePartner = txtPrimePartner.Text.Trim(); ;
        }
        else
        {
            sPrimePartner = "";
        }

    }
    protected void btnClearAll_Click(object sender, EventArgs e)
    {
         ActiveProjectList.SelectedIndex = 0;
        ddlCountry1.SelectedIndex = -1;
        cboProgramAreas.SelectedIndex = -1;
        cboAgency.SelectedIndex = -1;
        txtProjectName.Text = "";
        txtPrimePartner.Text = "";
        displayRecords();

    }
    protected void hideShow_Click(object sender, EventArgs e)
    {

        if (adiv.Style["display"] == "none")
        {
            adiv.Style["display"] = "block";
        }
        else
        {
            adiv.Style["display"] = "none";
        }
    }
}
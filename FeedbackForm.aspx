﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FeedbackForm.aspx.cs" Inherits="FeedbackForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="/../ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/themes/south-street/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>Feedback</h2>
    <div class="formLayout">

        <p>
            <label for="txtSubject"><font color="#FF0000">*</font>Subject:</label>
            <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqdSubject" ForeColor="Red" ValidationGroup="save"
                runat="server" ControlToValidate="txtSubject" ErrorMessage="Subject is missing">*</asp:RequiredFieldValidator>

        </p>
        <p>
            <label for="cboFeedbackType">
                <font color="#FF0000">*</font>Type of Feedback:
            </label>

            <asp:DropDownList ID="cboFeedbackType" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="reqedFeedbacktype" ForeColor="Red" ValidationGroup="save" InitialValue="Select"
                runat="server" ControlToValidate="cboFeedbackType" ErrorMessage="Type of feedback is missing">*</asp:RequiredFieldValidator>

        </p>
        <p>
            <label for="txtDescription"><font color="#FF0000">*</font>Description:</label>
            <asp:TextBox ID="txtDescription" runat="server" Height="182px" TextMode="MultiLine" Width="482px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqdDescription" ForeColor="Red" ValidationGroup="save"
                runat="server" ControlToValidate="txtDescription" ErrorMessage="Description is missing">*</asp:RequiredFieldValidator>
        </p>
        <p>
            <label for="txtURL">&nbsp; URL</label>
            <asp:TextBox ID="txtURL" runat="server" Width="485px"></asp:TextBox>
        </p>
        <p>
            <label for="txtPhone">&nbsp; Phone</label>
            <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
        </p>
        <p>
            <label for="txtEmail"><font color="#FF0000">*</font>Email</label>
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqdEmail" ForeColor="Red" ValidationGroup="save"
                runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is missing">*</asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Button ID="btnSave" runat="server" Text="Submit" ValidationGroup="save" OnClick="btnSave_Click" />&nbsp;
         <%--    <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" Visible="true" />&nbsp;&nbsp;&nbsp;   --%>       
        </p>
        <p>
            <asp:Label runat="server" ID="lblMessage" ForeColor="Red"></asp:Label>
        </p>



        <p>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="save" HeaderText="Required Fields:" ForeColor="Red" />
    </div>

</asp:Content>

